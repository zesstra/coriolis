/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.yearzeroengine.Talent.Type;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.SkillController;
import org.prelle.yearzeroengine.chargen.CommonCharacterGenerator;
import org.prelle.yearzeroengine.chargen.SkillGenerator;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CoriolisSkillGenerator extends SkillGenerator<CoriolisCharacter> implements
		SkillController {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;
	private final static PropertyResourceBundle RES = CoriolisCharGenConstants.RES;

	//--------------------------------------------------------------------
	public CoriolisSkillGenerator(
			CommonCharacterGenerator<CoriolisCharacter> parent) {
		super(parent);
	}
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = super.process(model, previous);

		/*
		 * Mystics with at least 1 in the skill "Mystic Powers" get one free
		 * mystical power talent. Grant one free talent - the constraint on
		 * mystical powers is applied in CoriolisTalentGenerator
		 */
//		boolean isMystic = parent.getCharacter().getSubconcept()!=null
//				&& parent.getCharacter().getSubconcept().getId().equals("mystic");
//		if (model.getSkillValue(model.getRuleset().getSkill("mystic_powers")).getPoints()>0 && isMystic)
//			unprocessed.add(new FreePointsModification(FreePointsModification.Type.TALENTS, 1));

		// Warn if character has mystic powers but not the skill
		boolean hasMystic = false;
		for (TalentValue val : model.getTalents()) {
			logger.info("  talent = "+val.getTalent());
			if (val.getTalent().getType()==Type.MYSTICAL)
				hasMystic = true;
		}
		for (Modification tmp : unprocessed) {
			if (tmp instanceof TalentModification && ((TalentModification)tmp).getTalent().getType()==Type.MYSTICAL)
				hasMystic = true;
		}
		logger.info(" hasMystic = "+hasMystic);

		if (hasMystic && model.getSkillValue(CoriolisCore.getRuleset().getSkill("mystic_powers")).getPoints()==0) {
			toDos.add(new ToDoElement(Severity.WARNING, RES.getString("todo.mystic_talent_without_skill")));
			logger.info("  added required mystic skill warning");
		}

		return unprocessed;
	}

}
