/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.HomeSystem;
import org.prelle.coriolis.HumanType;
import org.prelle.coriolis.Origin;
import org.prelle.coriolis.Upbringing;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class BackgroundGenerator implements CharacterProcessor {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;
	private PropertyResourceBundle RES;

	private CoriolisCharacterGenerator parent;
	protected List<ToDoElement> toDos;

	//-------------------------------------------------------------------
	/**
	 */
	public BackgroundGenerator(CoriolisCharacterGenerator parent) {
		this.parent = parent;
		toDos = new ArrayList<>();
		RES = parent.geti18nResources();
	}

	//-------------------------------------------------------------------
	public void setOrigin(Origin value) {
		((CoriolisCharacter)parent.getCharacter()).setOrigin(value);
		logger.info("Select origin: "+value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	public void setUpbringing(Upbringing value) {
		((CoriolisCharacter)parent.getCharacter()).setUpbringing(value);
		logger.info("Select upbringing: "+value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	public List<HumanType> getAvailableHumanTypes() {
		if ( ((CoriolisCharacter)parent.getCharacter()).getUpbringing()!=null &&
				((CoriolisCharacter)parent.getCharacter()).getUpbringing().getId().equals("privileged")) {
			return Arrays.asList( parent.getRuleset().getByType(HumanType.class, "human") );
		}
		return parent.getRuleset().getListByType(HumanType.class);
	}

	//-------------------------------------------------------------------
	public void setHumanType(HumanType value) {
		((CoriolisCharacter)parent.getCharacter()).setHumanType(value);
		logger.info("Select upbringing: "+value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	public void setHomeSystem(HomeSystem value) {
		((CoriolisCharacter)parent.getCharacter()).setHomeSystem(value);
		logger.info("Select homesystem: "+value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	public HomeSystem getRandomHomeSystem() {
		logger.info("Randomize homesystem");
		YearZeroEngineRuleset rules = parent.getRuleset();

		int d6 = (new Random()).nextInt(6);
		logger.debug("Dice is "+d6);
		switch (d6) {
		case 0: return rules.getByType(HomeSystem.class, "algol");
		case 1: return rules.getByType(HomeSystem.class, "mira");
		case 2: return rules.getByType(HomeSystem.class, "kua");
		case 3: return rules.getByType(HomeSystem.class, "dabaran");
		case 4: return rules.getByType(HomeSystem.class, "zalos");
		default:
			List<HomeSystem> options = rules.getListByType(HomeSystem.class);
			options.remove(rules.getByType(HomeSystem.class, "algol"));
			options.remove(rules.getByType(HomeSystem.class, "mira"));
			options.remove(rules.getByType(HomeSystem.class, "kua"));
			options.remove(rules.getByType(HomeSystem.class, "dabaran"));
			options.remove(rules.getByType(HomeSystem.class, "zalos"));
			int dice = (new Random()).nextInt(options.size());
			logger.debug("Options left="+options.size()+"   dice="+dice);
			return options.get(dice);
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter genModel, List<Modification> previous) {
		logger.trace("START: process");
		CoriolisCharacter model = (CoriolisCharacter)genModel;
		List<Modification> unprocessed = new ArrayList<>(previous);
		toDos.clear();
		try {
			// Origin
			if (model.getOrigin()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.no_origin_selected")));
			}
			// Homesystem
			if (model.getHomeSystem()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.no_homesystem_selected")));
			}
			// Upbringing
			if (model.getUpbringing()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.no_upbringing_selected")));
			} else {
				logger.debug("Add mods = "+model.getUpbringing().getModifications());
				unprocessed.addAll(model.getUpbringing().getModifications());
			}
			// Humanites
			if (model.getHumanType()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.no_humantype_selected")));
			} else {
				logger.debug("Add mods = "+model.getHumanType().getModifications());
				unprocessed.addAll(model.getHumanType().getModifications());
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
