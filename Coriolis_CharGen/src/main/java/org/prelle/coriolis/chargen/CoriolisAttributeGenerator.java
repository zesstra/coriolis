/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.Upbringing;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.chargen.AttributeGenerator;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.AttributeModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CoriolisAttributeGenerator extends AttributeGenerator<CoriolisCharacter> implements AttributeController, CharacterProcessor {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;
	private final static PropertyResourceBundle RES = CoriolisCharGenConstants.RES;

	//-------------------------------------------------------------------
	public CoriolisAttributeGenerator(CoriolisCharacterGenerator parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.AttributeGenerator#getMaximum(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public int getMaximum(Attribute key) {
		return (key==keyAttr)?5:4;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#canBeDecreased(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue val) {
		return (val.getPoints()>2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> ret = super.process(model, previous);

		try {
			logger.info("verify attributes with key attr "+keyAttr);
			/*
			 * Verify attribute values
			 */
			for (Attribute attr : AttributeCoriolis.primaryValues()) {
				AttributeValue val = model.getAttribute(attr);
				if (val.getBought()<2) {
					toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.attribute.below_minval")));
				} else if (val.getBought()>4 && attr!=keyAttr) {
					toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.attribute.above_maxval")));
				}
			}

			/*
			 * Half reputation from upbringing, if char is humanite
			 */
			if (((CoriolisCharacter)model).getHumanType()!=null &&
					!((CoriolisCharacter)model).getHumanType().getId().equals("human")) {
				logger.debug("Char is humanite - half upbringing reputation");
				AttributeValue rep = model.getAttribute(AttributeCoriolis.REPUTATION);
				for (Modification tmp : rep.getModifications()) {
					if (tmp instanceof AttributeModification && tmp.getSource() instanceof Upbringing) {
						AttributeModification mod = (AttributeModification)tmp;
						mod.setValue( mod.getValue() / 2 );
					}
				}
			}


			/*
			 * Derived values
			 */
			ret.add(new FreePointsModification(Type.ENCUMBRANCE, model.getAttribute(AttributeCoriolis.STRENGTH).getModifiedValue()*2));
//			AttributeValue hit = model.getAttribute(AttributeCoriolis.HIT_POINTS);
//			AttributeValue mind = model.getAttribute(AttributeCoriolis.MIND_POINTS);
//			hit.setPoints(model.getAttribute(AttributeCoriolis.STRENGTH).getModifiedValue()+
//					model.getAttribute(AttributeCoriolis.AGILITY).getModifiedValue());
//			logger.info("  Set HIT_POINTS to "+hit.getPoints());
//			mind.setPoints(model.getAttribute(AttributeCoriolis.EMPATHY).getModifiedValue()+
//					model.getAttribute(AttributeCoriolis.WITS).getModifiedValue());
//			logger.info("  Set MIND_POINTS to "+mind.getPoints());
		} finally {
			logger.trace("STOP : process");
		}

		return ret;
	}
}
