/**
 *
 */
package org.prelle.coriolis.charlvl;

import java.util.PropertyResourceBundle;

import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.chargen.CoriolisCharGenConstants;
import org.prelle.coriolis.charproc.CalculateDerivedAttributes;
import org.prelle.yearzeroengine.charlvl.CommonCharacterLeveller;
import org.prelle.yearzeroengine.charproc.ApplyTalentModifications;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

/**
 * @author Stefan
 *
 */
public class CoriolisCharacterLeveller extends CommonCharacterLeveller<CoriolisCharacter> implements CoriolisCharacterController {

	private final static PropertyResourceBundle RES = CoriolisCharGenConstants.RES;

	//--------------------------------------------------------------------
	public CoriolisCharacterLeveller(CoriolisCharacter model) {
		super(CoriolisCore.getRuleset(), RES, model);

		attributes = new CoriolisAttributeLeveller(this);
		talents = new CoriolisTalentLeveller(this);
		skills  = new CoriolisSkillLeveller(this);
		gear    = new CoriolisGearLeveller(this);

		processChain.add( (CharacterProcessor) talents);
		processChain.add( (CharacterProcessor) attributes);
		processChain.add( (CharacterProcessor) skills);
		processChain.add( (CharacterProcessor) gear);
//		processChain.add( (CharacterProcessor) fluff);
//		processChain.add( ((AttributeGenerator<?>)attributes).getCostCalculator() );
//		processChain.add( ((SkillGenerator<?>)skills).getCostCalculator() );
//		processChain.add( ((TalentGenerator<?>)talents).getCostCalculator() );

		processChain.add(new CalculateDerivedAttributes());
		processChain.add(new ApplyTalentModifications());

		runProcessors();
	}

}
