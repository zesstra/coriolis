/**
 *
 */
package org.prelle.coriolis.charlvl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisTools;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charlvl.TalentLeveller;
import org.prelle.yearzeroengine.modifications.TalentModification;

/**
 * @author Stefan
 *
 */
public class CoriolisTalentLeveller extends TalentLeveller<CoriolisCharacter> {

	private final static Logger logger = LogManager.getLogger("coriolis.charlvl");

	//--------------------------------------------------------------------
	public CoriolisTalentLeveller(CoriolisCharacterLeveller parent) {
		super(parent);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeSelected(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean canBeSelected(Talent key) {
		boolean generic = super.canBeSelected(key);
		if (!generic)
			return false;
		
		if (parent.getCharacter().getExpFree()<5)
			return false;
		
		switch (key.getType()) {
		case BIONIC:
		case CYBERNETIC:
		case GENERAL:
			return true;
		case MYSTICAL:
			return parent.getCharacter().getSkillValue(parent.getRuleset().getSkill("mystic_powers")).getPoints()>0;
		case HUMANITE:
		case ICON:
		case GROUP:
			return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeDeselected(org.prelle.yearzeroengine.TalentValue)
	 */
	@Override
	public boolean canBeDeselected(TalentValue key) {
		boolean generic = super.canBeDeselected(key);
		if (!generic)
			return false;
		switch (key.getTalent().getType()) {
		case BIONIC:
		case CYBERNETIC:
		case GENERAL:
		case MYSTICAL:
			return true;
		case HUMANITE:
		case ICON:
		case GROUP:
			return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#select(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean select(Talent key) {
		if (!canBeSelected(key)) {
			logger.warn("Trying to select talent "+key+" which cannot be selected");
			return false;
		}

		YearZeroEngineCharacter model = parent.getCharacter();

		TalentValue val = new TalentValue(key, 0);
		model.addTalent(val);
		logger.info("Selected talent "+key);
		// Add to history
		parent.getCharacter().addToHistory(new TalentModification(val.getTalent(), 5));

		// Pay exp
		model.setExpFree(model.getExpFree()-5);
		model.setExpInvested(model.getExpInvested()+5);

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#deselect(org.prelle.yearzeroengine.TalentValue)
	 */
	@Override
	public boolean deselect(TalentValue key) {
		if (!canBeDeselected(key)) {
			logger.warn("Trying to deselect talent "+key+" which cannot be deselected");
			return false;
		}

		YearZeroEngineCharacter model = parent.getCharacter();

		// Remove to history
		CoriolisTools.removeFromHistory(parent.getCharacter(), new TalentModification(key.getTalent(), 5));
		// Apply
		model.removeTalent(key);
		logger.info("Deselected talent "+key);

		// Pay exp
		model.setExpFree(model.getExpFree()+5);
		model.setExpInvested(model.getExpInvested()-5);

		parent.runProcessors();
		return true;
	}

}
