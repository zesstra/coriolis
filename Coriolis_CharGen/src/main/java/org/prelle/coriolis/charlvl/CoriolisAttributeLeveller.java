/**
 *
 */
package org.prelle.coriolis.charlvl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.chargen.CoriolisCharGenConstants;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charlvl.CommonCharacterLeveller;
import org.prelle.yearzeroengine.charlvl.NoOpAttributeLeveller;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;
import org.prelle.yearzeroengine.modifications.KeyAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CoriolisAttributeLeveller extends NoOpAttributeLeveller<CoriolisCharacter> {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;

	protected CommonCharacterLeveller<CoriolisCharacter> parent;

	//-------------------------------------------------------------------
	public CoriolisAttributeLeveller(CommonCharacterLeveller<CoriolisCharacter> parent) {
		this.parent = parent;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue value) {
		if (value.getAttribute()==AttributeCoriolis.RADIATION || value.getAttribute()==AttributeCoriolis.MENTAL_DAMAGE)
			return true;
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue value) {
		if (value.getModifiedValue()<=0)
			return false;
		if (value.getAttribute()==AttributeCoriolis.RADIATION || value.getAttribute()==AttributeCoriolis.MENTAL_DAMAGE)
			return true;
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#increase(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public boolean increase(AttributeValue val) {
		if (!canBeIncreased(val)) {
			logger.warn("Trying to increase attribute "+val.getModifyable()+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()+1);
		logger.info("Increase "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#decrease(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public boolean decrease(AttributeValue val) {
		if (!canBeDecreased(val)) {
			logger.warn("Trying to decrease attribute "+val.getModifyable()+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()-1);
		logger.info("Decrease "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		try {
			// Ensure HP and MP are present (and set to 0)
			((CoriolisCharacter)model).initAttributes(AttributeCoriolis.derivedValues());
			// Calculcate HP and MP
			model.getAttribute(AttributeCoriolis.HIT_POINTS).setPoints(
					model.getAttribute(AttributeCoriolis.STRENGTH).getPoints()+
					model.getAttribute(AttributeCoriolis.AGILITY).getPoints()
					);
			logger.debug("HIT_POINTS  = "+model.getAttribute(AttributeCoriolis.HIT_POINTS).getPoints());
			model.getAttribute(AttributeCoriolis.MIND_POINTS).setPoints(
					model.getAttribute(AttributeCoriolis.EMPATHY).getPoints()+
					model.getAttribute(AttributeCoriolis.WITS).getPoints()
					);
			logger.debug("MIND_POINTS = "+model.getAttribute(AttributeCoriolis.MIND_POINTS).getPoints());

			for (Modification tmp : super.process(model, previous)) {
				if (tmp instanceof KeyAttributeModification) {
					KeyAttributeModification mod = (KeyAttributeModification)tmp;
					keyAttr = mod.getAttribute();
					logger.debug(" Key attribute is "+keyAttr);
					continue;
				}
				unprocessed.add(tmp);
			}

			/*
			 * Derived values
			 */
			unprocessed.add(new FreePointsModification(Type.ENCUMBRANCE, model.getAttribute(AttributeCoriolis.STRENGTH).getModifiedValue()*2));
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

}
