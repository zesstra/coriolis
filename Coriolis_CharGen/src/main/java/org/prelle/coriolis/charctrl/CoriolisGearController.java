/**
 *
 */
package org.prelle.coriolis.charctrl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.CoriolisItemTemplate.Weight;
import org.prelle.coriolis.SelectOption;
import org.prelle.coriolis.Selection;
import org.prelle.coriolis.TechTier;
import org.prelle.coriolis.chargen.CoriolisCharGenConstants;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.charctrl.GearController;

/**
 * @author Stefan
 *
 */
public interface CoriolisGearController extends GearController {
	
	public final static Logger logger = CoriolisCharGenConstants.LOGGGER;

	//-------------------------------------------------------------------
	public CoriolisCarriedItem select(CoriolisItemTemplate item, ItemLocation loc, Selection... choices);

	//-------------------------------------------------------------------
	public static Selection[] parseAsSelectionString(String option) {
		logger.info("parseAsSelectionString:"+option);
		List<Selection> sel = new ArrayList<Selection>();
		if (option!=null) {
			StringTokenizer tok = new StringTokenizer(option, ", ");
			while (tok.hasMoreTokens()) {
				String pair_s = tok.nextToken();
				String[] pair = pair_s.split("=");
				Selection toAdd = new Selection();
				toAdd.option = SelectOption.valueOf(pair[0]);
				switch (toAdd.option) {
				case WEIGHT:
					toAdd.value = Weight.valueOf(pair[1]);
					break;
				case TIER:
					toAdd.value = TechTier.valueOf(pair[1]);
					break;
				case COST:
				case COUNT:
				case VAL:
					toAdd.value = Integer.parseInt(pair[1]);
					break;
				case SKILL:
					toAdd.value = CoriolisCore.getRuleset().getSkill(pair[1]);
					break;
				}
				sel.add(toAdd);
			}
		}

		Selection[] ret = new Selection[sel.size()];
		ret = sel.toArray(ret);
		return ret;
	}
}
