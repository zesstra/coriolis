/**
 *
 */
package org.prelle.coriolis.charctrl;

import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.yearzeroengine.charctrl.CharacterController;

/**
 * @author Stefan
 *
 */
public interface CoriolisCharacterController extends CharacterController<CoriolisCharacter> {

}
