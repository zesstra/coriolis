/**
 *
 */
package coriolis;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.CoriolisItemTemplate.Weight;
import org.prelle.coriolis.TechTier;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.charlvl.CoriolisGearLeveller;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class GearLevellerTest {

	private CoriolisGearLeveller gear;
	private CoriolisCharacter model;

	//-------------------------------------------------------------------
	static {
		CoriolisCore.initialize(new DummyRulePlugin<>());
//		System.exit(0);

	}

	//--------------------------------------------------------------------
	@Before
	public void setUp() {
		CoriolisCharacterGenerator parent = new CoriolisCharacterGenerator();
		model = parent.getCharacter();
		gear = new CoriolisGearLeveller(parent);
	}

	//--------------------------------------------------------------------
	@Test
	public void testIdle() {
		assertEquals(0, gear.getEncumbrancePointsLeft(), 0);
	}

	//--------------------------------------------------------------------
	@Test
	public void testAcceptPoints() {
		List<Modification> previous = new ArrayList<>();
		previous.add(new FreePointsModification(Type.ENCUMBRANCE, 6));
		gear.process(null, previous);
		assertEquals(6, gear.getEncumbrancePointsLeft(), 0);
	}

	//--------------------------------------------------------------------
	@Test
	public void testPointCalculation() {
		CoriolisItemTemplate template0 = new CoriolisItemTemplate("t0", TechTier.O, Weight.TINY);
		CoriolisItemTemplate template1 = new CoriolisItemTemplate("t1", TechTier.O, Weight.LIGHT);
		CoriolisItemTemplate template2 = new CoriolisItemTemplate("t2", TechTier.O, Weight.NORMAL);
		CoriolisItemTemplate template3 = new CoriolisItemTemplate("t3", TechTier.O, Weight.HEAVY);
		model.addItem(new CoriolisCarriedItem(template0)); // 0
		model.addItem(new CoriolisCarriedItem(template1)); // 0.5
		model.addItem(new CoriolisCarriedItem(template2)); // 1.0
		model.addItem(new CoriolisCarriedItem(template3)); // 2.0

		List<Modification> previous = new ArrayList<>();
		previous.add(new FreePointsModification(Type.ENCUMBRANCE, 6));
		gear.process(null, previous);
		assertEquals("Encumbrance not calculated", 2.5, gear.getEncumbrancePointsLeft(), 0);
	}

}
