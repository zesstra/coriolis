/**
 *
 */
package org.prelle.coriolis.jfx;

import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.listcells.TalentListCell;
import org.prelle.coriolis.jfx.listcells.TalentValueListCell;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroModule;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class Talent2ColumnView extends HBox {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterController ctrl;

	private ListView<Talent> lvAvailable;
	private ListView<TalentValue> lvSelected;

	private ObjectProperty<YearZeroModule> showHelpForProperty;

	//--------------------------------------------------------------------
	public Talent2ColumnView(CoriolisCharacterController ctrl) {
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		showHelpForProperty = new SimpleObjectProperty<>();
		lvAvailable = new ListView<Talent>();
		lvAvailable.setCellFactory(lv -> new TalentListCell(ctrl));
		Label placeholder = new Label(RES.getString("label.nothing_to_select"));
		placeholder.setWrapText(true);
		lvAvailable.setPlaceholder(placeholder);
		lvSelected  = new ListView<TalentValue>();
		lvSelected.getItems().addAll(ctrl.getCharacter().getTalents());
		lvSelected.setCellFactory(lv -> new TalentValueListCell(ctrl));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbAvailable = new Label(RES.getString("label.available"));
		Label lbSelected  = new Label(RES.getString("label.selected"));
		lbAvailable.getStyleClass().add("base");
		lbSelected.getStyleClass().add("base");

		VBox col1 = new VBox(lbAvailable, lvAvailable);
		col1.setStyle("-fx-spacing: 0.5em");
		VBox col2 = new VBox(lbSelected, lvSelected);
		col2.setStyle("-fx-spacing: 0.5em");

		getChildren().addAll(col1, col2);
		setStyle("-fx-spacing: 2em");
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)
				-> showHelpForProperty.set(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)
				-> {if (n!=null) showHelpForProperty.set(n.getTalent());});

		lvSelected.setOnDragOver(ev -> dragOverSelected(ev));
		lvSelected.setOnDragDropped(ev -> dragDroppedSelected(ev));
		lvAvailable.setOnDragOver(ev -> dragOverAvailable(ev));
		lvAvailable.setOnDragDropped(ev -> dragDroppedAvailable(ev));
	}

	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<YearZeroModule> showHelpForProperty() {
		return showHelpForProperty;
	}

	//--------------------------------------------------------------------
	public void refresh() {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getTalentController().getAvailableTalents());
		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(ctrl.getCharacter().getTalents());
	}

	//-------------------------------------------------------------------
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			if (event.getDragboard().getString().startsWith("talent:"))
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	private void dragDroppedSelected(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);
			// Get reference for ID
			if (enhanceID.startsWith("talent:")) {
				StringTokenizer tok = new StringTokenizer(enhanceID.substring(7),"|");
				String talID = tok.nextToken();
				//        		String descr = tok.nextToken();
				//        		String idref = tok.nextToken();
				//        		if (descr!=null) descr=descr.substring(5);
				//        		if (idref!=null) idref=idref.substring(6);
				//        		if ("null".equals(descr)) descr=null;
				//        		if ("null".equals(idref)) idref=null;

				Talent toMove = ctrl.getRuleset().getByType(Talent.class, talID);
				logger.debug("Select talent "+toMove);
				if (ctrl.getTalentController().select(toMove)) {
					logger.info("Selected talent "+toMove);
				} else {
					logger.warn("Failed selecting talent: "+toMove);
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			if (event.getDragboard().getString().startsWith("talentref:"))
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	private void dragDroppedAvailable(DragEvent event) {
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);
			// Get reference for ID
			if (enhanceID.startsWith("talentref:")) {
				StringTokenizer tok = new StringTokenizer(enhanceID.substring(10),"|");
				String talID = tok.nextToken();

				Talent talent = ctrl.getRuleset().getByType(Talent.class, talID);
				TalentValue toMove = ctrl.getCharacter().getTalentValue(talent);
				logger.debug("Deselect talent "+toMove);
				if (ctrl.getTalentController().deselect(toMove)) {
					logger.info("Deselected talent "+toMove);
				} else {
					logger.warn("Failed deselecting talent: "+toMove);
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

}
