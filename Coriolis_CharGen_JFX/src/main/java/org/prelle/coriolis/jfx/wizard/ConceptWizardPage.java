/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisTools;
import org.prelle.coriolis.SubConcept;
import org.prelle.coriolis.charctrl.CoriolisCharacterClassController;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.AttributeTable;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;
import org.prelle.yearzeroengine.modifications.ItemModification;
import org.prelle.yearzeroengine.modifications.ModificationChoice;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;




/**
 * @author Stefan
 *
 */
public class ConceptWizardPage extends WizardPage implements GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private ComboBox<CharacterClass> cbConcept;
	private ComboBox<SubConcept> cbSubConcept;
	private VBox bxDecisions;

	private FreePointsNode lbPointsLeft;
	private AttributeTable tbAttributes;

	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	public ConceptWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.concept.title"));
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		cbConcept = new ComboBox<>();
		cbConcept.getItems().addAll(ctrl.getRuleset().getCharacterClasses());
		cbConcept.setConverter(new StringConverter<CharacterClass>() {
			public String toString(CharacterClass value) { return (value!=null)?value.getName():null;}
			public CharacterClass fromString(String arg0) { return null; }
		});
		cbConcept.setCellFactory( lv -> new ConceptListCell(ctrl));

		cbSubConcept = new ComboBox<>();
		cbSubConcept.getItems().addAll(ctrl.getRuleset().getListByType(SubConcept.class));
		cbSubConcept.setConverter(new StringConverter<SubConcept>() {
			public String toString(SubConcept value) { return (value!=null)?value.getName():null;}
			public SubConcept fromString(String arg0) { return null; }
		});
		cbSubConcept.setCellFactory( lv -> new SubConceptListCell(ctrl));

		bxDecisions = new VBox();
		bxDecisions.setStyle("-fx-spacing: 0.5em");

		/*
		 * Attributes
		 */
		lbPointsLeft = new FreePointsNode();
		lbPointsLeft.setName(RES.getString("label.points.short"));
		lbPointsLeft.setStyle("-fx-font-size: +200%");
		tbAttributes = new AttributeTable(ctrl, AttributeCoriolis.primaryValues());

		/*
		 * Explain
		 */
		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectName.setWrapText(true);
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.concept.intro"));
		lbIntro.setWrapText(true);

		/*
		 * CharacterClass
		 */
		Label hdCharacterClass = new Label(RES.getString("wizard.concept.concept"));
		hdCharacterClass.getStyleClass().add("subtitle");
		Label lbConcept = new Label(RES.getString("wizard.concept.concept"));
		lbConcept.getStyleClass().add("base");
		Label lbSubConcept = new Label(RES.getString("wizard.concept.subconcept"));
		lbSubConcept.getStyleClass().add("base");

		/*
		 * Decisions
		 */
		Label hdDecisions = new Label(RES.getString("wizard.concept.decisions"));
		hdDecisions.getStyleClass().add("base");

		GridPane bxCharacterClass = new GridPane();
		bxCharacterClass.add(lbConcept    , 0, 0);
		bxCharacterClass.add(cbConcept    , 1, 0);
		bxCharacterClass.add(hdDecisions  , 2, 0);
		bxCharacterClass.add(bxDecisions  , 3, 0);
		bxCharacterClass.add(lbSubConcept , 0, 1);
		bxCharacterClass.add(cbSubConcept , 1, 1);
		//		bxCharacterClass.add(btnSubConcept, 2, 1);
		bxCharacterClass.setStyle("-fx-vgap: 1em; -fx-hgap: 0.5em");
		bxCharacterClass.setAlignment(Pos.CENTER_LEFT);

		/*
		 * Attributes
		 */
		Label hdAttributes = new Label(RES.getString("wizard.attributes.title"));
		hdAttributes.getStyleClass().add("subtitle");
		Label lbIntro2 = new Label(RES.getString("wizard.attributes.intro"));
		lbIntro2.setWrapText(true);

		HBox pointsAndIntro = new HBox();
		pointsAndIntro.getChildren().addAll(lbPointsLeft, lbIntro2);
		pointsAndIntro.setAlignment(Pos.CENTER_LEFT);
		pointsAndIntro.setStyle("-fx-spacing: 2em;");

		/*
		 * Content
		 */
		GridPane conceptsAndDec = new GridPane();

		VBox content = new VBox(5);
		content.getChildren().addAll(lbIntro);
		content.getChildren().addAll(hdCharacterClass, bxCharacterClass, hdAttributes, pointsAndIntro, tbAttributes);
		VBox.setMargin(hdCharacterClass, new Insets(20, 0, 10, 0));
		VBox.setMargin(hdAttributes, new Insets(20, 0, 10, 0));

		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, line, explain);
		allContent.setStyle("-fx-spacing: 1em");
//		content.setStyle("-fx-pref-width: 60em; -fx-min-width:35em");
//		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:15em");
		content.setStyle("-fx-pref-width: 55em; -fx-min-width:35em");
		explain.setStyle("-fx-pref-width: 12em; -fx-min-width:10em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void updateHelp(YearZeroModule n) {
		if (n==null) {
			lbSelectName.setText(null);
			lbSelectRef.setText(null);
			lbSelectDesc.setText(null);
		} else {
			lbSelectName.setText(n.getName());
			lbSelectRef.setText(n.getProductName()+" "+n.getPage());
			lbSelectDesc.setText(n.getHelpText());
		}
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbConcept.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getCharacterClassController().setSelected(n);
			if (n!=null) {
				updateHelp(n);
				//				lbSelectName.setText(n.getName());
				//				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				//				lbSelectDesc.setText(n.getHelpText());
				//
				updateDecisions();
				// Get available subconcepts
				List<SubConcept> avail = new ArrayList<>();
				for (SubConcept sub : ctrl.getRuleset().getListByType(SubConcept.class)) {
					if (sub.getConcept()==n)
						avail.add(sub);
				}
				logger.info("update choicebox "+avail);
				cbSubConcept.getItems().clear();
				cbSubConcept.getItems().addAll(avail);
			}
		});

		cbSubConcept.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			((CoriolisCharacterClassController)ctrl.getCharacterClassController()).setSelectedSubConcept(n);
			if (n!=null) {
				updateHelp(n);
			}
		});

		//		btnSubConcept.setOnAction(ev -> {
		//			cbSubConcept.getSelectionModel().select(ctrl.getUpbringingController().getRandomSubConcept());
		//		});
	}

	//--------------------------------------------------------------------
	private void updateDecisions() {
		bxDecisions.getChildren().clear();

		List<DecisionToMake> decide = ctrl.getCharacterClassController().getDecisions();
		for (DecisionToMake choose : decide) {
			HBox line = new HBox();
			line.setStyle("-fx-spacing: 1em");
			ToggleGroup group = new ToggleGroup();
			group.setUserData(choose);
			if (choose.getChoice()==null) {
				logger.error("No choice for concept "+ctrl.getCharacterClassController().getSelected());
				return;
			}

			ModificationChoice choice = (ModificationChoice) choose.getChoice();

			/* Find out if there are only talent choices */
			boolean hasOnlyTalentMods = true;
			for (Modification mod : choice.getOptionList()) {
				if (!(mod instanceof TalentModification))
					hasOnlyTalentMods = false;
			}

			// For 1-3 choices use ToggleButtons, otherwise ChoiceBox
			if (choice.getOptionList().size()<=3) {
				for (Modification mod : choice.getOptionList()) {
					String name = CoriolisTools.toString(mod);
					ToggleButton btn = new ToggleButton(name);
					btn.setToggleGroup(group);
					btn.setUserData(mod);
					line.getChildren().add(btn);
					// Eventually set
					boolean selected = (choose.getDecision()!=null)?choose.getDecision().contains(mod):false;
					logger.debug("********** is option "+mod+" decided = "+selected);
					btn.setSelected(selected);
				}
				// Interactivity
				group.selectedToggleProperty().addListener( (ov,o,n) -> {
					if (n==null)
						return;
					Modification choosen = (Modification)n.getUserData();
					DecisionToMake dec = (DecisionToMake)n.getToggleGroup().getUserData();
					ctrl.getCharacterClassController().decide(dec, choosen);
					if (choosen instanceof TalentModification) {
						updateHelp( ((TalentModification)choosen).getTalent());
					} else if (choosen instanceof ItemModification) {
						updateHelp( ((ItemModification)choosen).getItem());
					} else
						logger.warn("Don't know how to show help for "+choosen.getClass());
				});
			} else {
				// 4 or more options
				ChoiceBox<Modification> cbChoices = new ChoiceBox<>();
				cbChoices.getItems().addAll(choice.getOptionList());
				cbChoices.setUserData(choose);
				cbChoices.setConverter(new StringConverter<Modification>() {
					public String toString(Modification mod) {
						return CoriolisTools.toString(mod);
					}
					public Modification fromString(String string) { return null; }
				});
				// Set current decision
				if (choose.getDecision()!=null) {
					for (Modification mod : choose.getDecision())
						cbChoices.getSelectionModel().select(mod);
				}

				line.getChildren().add(cbChoices);

				// Interactivity
				cbChoices.getSelectionModel().selectedItemProperty().addListener( (ov,o,choosen) -> {
					if (choosen==null)
						return;
					DecisionToMake dec = choose; //(DecisionToMake)n.getToggleGroup().getUserData();
					ctrl.getCharacterClassController().decide(dec, choosen);
					if (choosen instanceof TalentModification) {
						updateHelp( ((TalentModification)choosen).getTalent());
					} else if (choosen instanceof ItemModification) {
						updateHelp( ((ItemModification)choosen).getItem());
					} else
						logger.warn("Don't know how to show help for "+choosen.getClass());
				});
			}
			// Accept only the pure talent mod options here
			if (hasOnlyTalentMods)
				bxDecisions.getChildren().add(line);
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			int points = ctrl.getAttributeController().getPointsToSpend();
			lbPointsLeft.setPoints(points);
			tbAttributes.refresh();
			updateDecisions();
			break;
		default:
		}
	}

}

class ConceptListCell extends ListCell<CharacterClass> {

	private CoriolisCharacterGenerator ctrl;

	private Label icon;
	private Label text;
	private HBox content;

	//--------------------------------------------------------------------
	public ConceptListCell(CoriolisCharacterGenerator ctrl) {
		this.ctrl = ctrl;

		icon = new Label();
		icon.setStyle("-fx-text-fill: recommendation; -fx-min-width: 1em; -fx-pref-width: 1.5em");
		text = new Label();

		content = new HBox(5, icon, text);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CharacterClass item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(content);
			text.setText(item.getName());
			if ( ((CoriolisCharacterClassController)ctrl.getCharacterClassController()).isRecommended(item)) {
				icon.setText("!");
			} else {
				icon.setText(null);
			}
		}
	}
}


class SubConceptListCell extends ListCell<SubConcept> {

	private CoriolisCharacterGenerator ctrl;

	private Label icon;
	private Label text;
	private HBox content;

	//--------------------------------------------------------------------
	public SubConceptListCell(CoriolisCharacterGenerator ctrl) {
		this.ctrl = ctrl;

		icon = new Label();
		text = new Label();

		content = new HBox(5, icon, text);
		icon.setStyle("-fx-text-fill: recommendation; -fx-min-width: 1em; -fx-pref-width: 1.5em");
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SubConcept item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(content);
			text.setText(item.getName());
			if ( ((CoriolisCharacterClassController)ctrl.getCharacterClassController()).isRecommended(item)) {
				icon.setText("!");
			} else {
				icon.setText(null);
			}
		}
	}

}