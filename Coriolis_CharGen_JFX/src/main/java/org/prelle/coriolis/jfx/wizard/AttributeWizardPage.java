/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.AttributeTable;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;




/**
 * @author Stefan
 *
 */
public class AttributeWizardPage extends WizardPage implements GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private FreePointsNode lbPointsLeft;
	private AttributeTable tbAttributes;

	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	public AttributeWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.attributes.title"));
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lbPointsLeft = new FreePointsNode();
		lbPointsLeft.setStyle("-fx-font-size: +200%");
		tbAttributes = new AttributeTable(ctrl, AttributeCoriolis.primaryValues());

		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.attributes.intro"));
		lbIntro.setWrapText(true);

		HBox pointsAndIntro = new HBox();
		pointsAndIntro.getChildren().addAll(lbPointsLeft, lbIntro);
		pointsAndIntro.setAlignment(Pos.CENTER_LEFT);
		pointsAndIntro.setStyle("-fx-spacing: 2em;");

		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		Region grow = new Region();
		grow.setMaxHeight(Double.MAX_VALUE);
		VBox content = new VBox(20,pointsAndIntro, tbAttributes);


		HBox allContent = new HBox(content, line, explain);
		allContent.setStyle("-fx-spacing: 2em");
		pointsAndIntro.setStyle("-fx-pref-width: 40em; -fx-min-width:25em; -fx-spacing: 2em");
		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:15em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void updateHelp(YearZeroModule n) {
		lbSelectName.setText(n.getName());
		lbSelectRef.setText(n.getProductName()+" "+n.getPage());
		lbSelectDesc.setText(n.getHelpText());
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			int points = ctrl.getAttributeController().getPointsToSpend();
			lbPointsLeft.setPoints(points);
			tbAttributes.refresh();
			break;
		default:
		}

	}
}

