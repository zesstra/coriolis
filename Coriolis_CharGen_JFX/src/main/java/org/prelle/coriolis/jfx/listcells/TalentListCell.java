/**
 *
 */
package org.prelle.coriolis.jfx.listcells;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;

import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.charctrl.TalentController;

/**
 * @author Stefan
 *
 */
public class TalentListCell extends ListCell<Talent> {

	private CharacterController<?> parent;
	private TalentController control;

	private VBox layout;
	private Label lbName, lbAdditional;

	//--------------------------------------------------------------------
	public TalentListCell(CharacterController<?> ctrl) {
		this.parent = ctrl;
		this.control = ctrl.getTalentController();

		lbName = new Label(); lbName.getStyleClass().add("base");
		lbAdditional = new Label();

		layout = new VBox(5, lbName, lbAdditional);
		layout.setAlignment(Pos.TOP_LEFT);

		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Talent item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(layout);
			lbName.setText(item.getName());
			lbAdditional.setText(item.getProductNameShort()+" "+item.getPage());
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Talent data = itemProperty().get();
//		logger.debug("drag started for "+data);
		if (data==null)
			return;
//		logger.debug("canBeDeselected = "+parent.getTalentController().canBeDeselected(data));
		if (!parent.getTalentController().canBeSelected(data))
			return;

		Node source = (Node) event.getSource();
//		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);

        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        String id = "talent:"+data.getId();
        content.putString(id);
        db.setContent(content);

        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);

        event.consume();
    }

}
