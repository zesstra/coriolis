/**
 *
 */
package org.prelle.coriolis.jfx.sections;

import java.util.PropertyResourceBundle;

import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.CharacterController;

import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AppearanceSection extends SingleSection {

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;

	private CharacterController<? extends YearZeroEngineCharacter> ctrl;

	private TextArea tfFace;
	private TextArea tfClothing;

	private Label lbFace;
	private Label lbClothing;

	//-------------------------------------------------------------------
	/**
	 */
	public AppearanceSection(String title, CharacterController<? extends YearZeroEngineCharacter> gen, ScreenManagerProvider provider) {
		super(provider, title, null);
		this.ctrl = gen;
		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfFace = new TextArea();
		tfFace.setPrefColumnCount(30);
		tfFace.setPrefRowCount(2);
		tfClothing = new TextArea();
		tfClothing.setPrefColumnCount(30);
		tfClothing.setPrefRowCount(2);

		tfFace.setMaxWidth(Double.MAX_VALUE);
		tfClothing.setMaxWidth(Double.MAX_VALUE);

		lbFace = new Label(UI.getString("label.face"));
		lbClothing = new Label(UI.getString("label.clothing"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox.setHgrow(tfFace, Priority.ALWAYS);
		HBox.setHgrow(tfClothing, Priority.ALWAYS);

		GridPane grid = new GridPane();
		grid.add(lbFace        , 0, 0);
		grid.add(tfFace        , 1, 0);
		grid.add(lbClothing    , 0, 1);
		grid.add(tfClothing    , 1, 1);
		grid.setStyle("-fx-vgap: 0.5em; -fx-padding: 0.3em; -fx-hgap: 1em");
		GridPane.setValignment(lbFace, VPos.TOP);
		GridPane.setValignment(lbClothing, VPos.TOP);

		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfFace.textProperty().addListener( (ov,o,n) -> ctrl.getCharacter().setFace(n));
		tfClothing.textProperty().addListener( (ov,o,n) -> ctrl.getCharacter().setClothing(n));
	}

	//-------------------------------------------------------------------
	public void refresh() {
		YearZeroEngineCharacter model = ctrl.getCharacter();
		tfClothing.setText(model.getClothing());
		tfFace.setText(model.getFace());
	}

}
