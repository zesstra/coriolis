/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.PropertyResourceBundle;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineCharacter.Gender;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;

import de.rpgframework.RPGFramework;
import de.rpgframework.genericrpg.ToDoElement;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class AppearanceWizardPage extends WizardPage implements GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private static Preferences CONFIG = Preferences.userRoot().node(RPGFramework.LAST_OPEN_DIR);

	private CoriolisCharacterGenerator ctrl;

	private TextField tfName;
	private ChoiceBox<YearZeroEngineCharacter.Gender> cbGender;
	private Button btFace;
	private TextField tfFace;
	private Button btClothing;
	private TextField tfClothing;
	
	private VBox bxToDos;

	private ImageView ivPortrait;
	private Label dummyPortrait;
	private Button btnPortrait;
	private Button btnDel;

	//--------------------------------------------------------------------
	public AppearanceWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.appearance.title"));
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbGender = new ChoiceBox<>();
		cbGender.getItems().addAll(Gender.values());
		cbGender.setConverter(new StringConverter<YearZeroEngineCharacter.Gender>() {
			public String toString(Gender val) { return val.toString(); }
			public Gender fromString(String arg0) {	return null; }
		});

		btFace = new Button(RES.getString("button.random"));
		tfFace = new TextField();
		btClothing = new Button(RES.getString("button.random"));
		tfClothing = new TextField();
		tfClothing.setPrefColumnCount(26);
		
		bxToDos = new VBox();

		ivPortrait = new ImageView();
		ivPortrait.setFitHeight(300);
		ivPortrait.setFitWidth(300);
		ivPortrait.setPreserveRatio(true);
		dummyPortrait = new Label("\uE8B8");
		dummyPortrait.setStyle("-fx-font-family: 'Segoe MLD2 Assets'");
		dummyPortrait.setAlignment(Pos.CENTER);
		dummyPortrait.setStyle("-fx-font-size: 60pt");
		dummyPortrait.setPrefSize(150, 150);
		btnPortrait = new Button(null, dummyPortrait);
		if (ctrl.getCharacter().getImage()!=null) {
			btnPortrait.setGraphic(ivPortrait);
			ivPortrait.setImage(new Image(new ByteArrayInputStream(ctrl.getCharacter().getImage())));
		}
		btnPortrait.getStyleClass().add("icon");

		Label icoTrash = new Label("\uE74D");
		icoTrash.getStyleClass().add("icon");
		btnDel = new Button(RES.getString("appearance.btn.delete_image"), icoTrash);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.appearance.intro"));
		lbIntro.setWrapText(true);

		Label hdName    = new Label(RES.getString("label.name"));
		Label hdGender  = new Label(RES.getString("label.gender"));
		Label hdFace    = new Label(RES.getString("label.face"));
		Label hdClothing= new Label(RES.getString("label.clothing"));
		Label hdPortrait= new Label(RES.getString("label.portrait"));

		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 1em; -fx-hgap: 0.5em");
		grid.add(hdName  , 0, 0);
		grid.add(tfName  , 1, 0, 2,1);
		grid.add(hdGender, 0, 1);
		grid.add(cbGender, 1, 1, 2,1);
		grid.add(hdFace  , 0, 2);
		grid.add(btFace  , 1, 2);
		grid.add(tfFace  , 2, 2);
		grid.add(hdClothing, 0, 3);
		grid.add(btClothing, 1, 3);
		grid.add(tfClothing, 2, 3);
		grid.add(bxToDos , 0, 4, 3,1);


		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");
		
		VBox buttons = new VBox(btnPortrait, btnDel);
		buttons.setStyle("-fx-spacing: 2em");


		HBox allContent = new HBox(grid, line, buttons);
		allContent.setStyle("-fx-spacing: 2em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		btnPortrait.setOnAction(ev -> openImage());
		btnDel.setOnAction(ev -> removeImage());
		tfName.textProperty().addListener( (ov,o,n) -> ctrl.getFluffController().setName(n));
		tfClothing.textProperty().addListener( (ov,o,n) -> ctrl.getFluffController().setClothing(n));
		tfFace.textProperty().addListener( (ov,o,n) -> ctrl.getFluffController().setFace(n));
		btClothing.setOnAction(ev -> ctrl.getCharacterClassController().rollRandomClothing(ctrl.getCharacter()));
		btFace.setOnAction(ev -> ctrl.getCharacterClassController().rollRandomFace(ctrl.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void openImage() {
		logger.debug("openImage");
		FileChooser chooser = new FileChooser();
		chooser.setTitle(RES.getString("appearance.filechooser.title"));
		String lastDir = CONFIG.get(RPGFramework.PROP_LAST_OPEN_IMAGE_DIR, System.getProperty("user.home"));
		chooser.setInitialDirectory(new File(lastDir));
		chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
            );
		File selection = chooser.showOpenDialog(new Stage());
		if (selection!=null) {
			CONFIG.put(RPGFramework.PROP_LAST_OPEN_IMAGE_DIR, selection.getParentFile().getAbsolutePath().toString());
			try {
				byte[] imgBytes = Files.readAllBytes(selection.toPath());
				ivPortrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
				btnPortrait.setText(null);
				btnPortrait.setGraphic(ivPortrait);
				ctrl.getCharacter().setImage(imgBytes);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null));
			} catch (IOException e) {
				logger.warn("Failed loading image from "+selection+": "+e);
			}
		}
	}

	//-------------------------------------------------------------------
	private void removeImage() {
		ivPortrait.setImage(null);
		btnPortrait.setText(null);
		btnPortrait.setGraphic(dummyPortrait);
		ctrl.getCharacter().setImage(null);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			tfName.setText(ctrl.getCharacter().getName());
			tfClothing.setText(ctrl.getCharacter().getClothing());
			tfFace.setText(ctrl.getCharacter().getFace());
			if (ctrl.getCharacter().getImage()!=null) {
				ivPortrait.setImage(new Image(new ByteArrayInputStream(ctrl.getCharacter().getImage())));
				btnPortrait.setGraphic(ivPortrait);
			} else {
				btnPortrait.setGraphic(dummyPortrait);
			}
			
			bxToDos.getChildren().clear();
			for (ToDoElement todo : ctrl.getToDos()) {
				Label toAdd = new Label(todo.getMessage());
				switch (todo.getSeverity()) {
				case STOPPER:
					toAdd.setStyle("-fx-text-fill: textcolor-warning");
					break;
				case WARNING:
					toAdd.setStyle("-fx-text-fill: orange");
					break;
				}
				bxToDos.getChildren().add(toAdd);
			}
			break;
		default:
		}
		
	}

}
