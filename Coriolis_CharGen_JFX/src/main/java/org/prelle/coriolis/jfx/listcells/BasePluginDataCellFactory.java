/**
 *
 */
package org.prelle.coriolis.jfx.listcells;

import org.prelle.yearzeroengine.BasePluginData;

/**
 * @author Stefan
 *
 */
public interface BasePluginDataCellFactory<T> {

	public BasePluginDataCell<T> generateCell(Class<T> cls);
}
