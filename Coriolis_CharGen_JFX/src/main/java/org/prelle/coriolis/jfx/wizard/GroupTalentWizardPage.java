/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisGroupController;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.yearzeroengine.Talent;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class GroupTalentWizardPage extends WizardPage {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private ChoiceBox<Talent> cbTalent;
	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	public GroupTalentWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.groupconcept2.title"));
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		cbTalent = new ChoiceBox<>();
		cbTalent.getItems().addAll(ctrl.getRuleset().getListByType(Talent.class).stream().filter(tal -> tal.getType()==Talent.Type.GROUP).collect(Collectors.toList()));
		cbTalent.setConverter(new StringConverter<Talent>() {
			public String toString(Talent value) { return value.getName();}
			public Talent fromString(String arg0) { return null; }
		});

		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.groupconcept2.intro"));
		lbIntro.setWrapText(true);

		/*
		 * Group talent
		 */
		Label hdTalent = new Label(RES.getString("label.group_talent"));
		hdTalent.getStyleClass().add("subtitle");
		Label lbTalentEx = new Label(RES.getString("label.group_talent.explain"));
		lbTalentEx.setWrapText(true);
		Label lbTalent = new Label(RES.getString("label.group_talent"));
		lbTalent.getStyleClass().add("base");

		HBox bxTalent = new HBox(lbTalent, cbTalent);
		bxTalent.setStyle("-fx-spacing: 1em");
		bxTalent.setAlignment(Pos.CENTER_LEFT);


		VBox content = new VBox();
		content.getChildren().addAll(lbIntro);
		content.getChildren().addAll(hdTalent, lbTalentEx, bxTalent);
		VBox.setMargin(hdTalent, new Insets(20, 0, 10, 0));


		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, line, explain);
//		content.setStyle("-fx-min-width:25em");
//		explain.setStyle("-fx-pref-width: 30em; -fx-min-width:15em");
		content.setStyle("-fx-min-width:25em");
		explain.setStyle("-fx-min-width:15em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbTalent.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			((CoriolisGroupController)ctrl.getGroupController()).setGroupTalent(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				lbSelectDesc.setText(n.getHelpText());
			}
		});

//		for (TextField tmp : tfName) {
//			tmp.textProperty().addListener( (ov,o,n) -> updateCharNames());
//		}
	}

}
