/**
 * 
 */
package org.prelle.coriolis.jfx.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.ItemType;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.listcells.ItemTemplateListCell;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelectorSkin extends SkinBase<ItemTemplateSelector> implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;

	private YearZeroEngineRuleset rules;
	private CoriolisCharacterController controller;
	private VBox content;
	private TextField tfName;
	private ChoiceBox<ItemType> cbTypes;
	private ListView<CoriolisItemTemplate> lvItems;
	
	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ItemTemplateSelectorSkin(ItemTemplateSelector control, CoriolisCharacterController controller) {
		super(control);
		this.rules = controller.getRuleset();
		this.controller = controller;
		
		initComponents(control.getAllowedTypes());
		initLayout();
		initInteractivity();
		cbTypes.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents(ItemLocation allowedTypes) {
		tfName = new TextField();
		tfName.setPromptText(UI.getString("itemselector.name.prompt"));

		cbTypes = new ChoiceBox<ItemType>();
		cbTypes.setMaxWidth(Double.MAX_VALUE);
		cbTypes.getItems().addAll(ItemType.values());
		cbTypes.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType data) {return data.getName();}
			public ItemType fromString(String data) {return null;}
		});

		lvItems = new ListView<CoriolisItemTemplate>();
		lvItems.setCellFactory(new Callback<ListView<CoriolisItemTemplate>, ListCell<CoriolisItemTemplate>>() {
			public ListCell<CoriolisItemTemplate> call(ListView<CoriolisItemTemplate> param) {
				return new ItemTemplateListCell(controller);
			}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new VBox();
		content.setStyle("-fx-spacing: 0.5em");
		content.getChildren().addAll(tfName, cbTypes, lvItems);
		VBox.setVgrow(lvItems, Priority.ALWAYS);

		lvItems.setStyle("-fx-pref-width: 30em");
		lvItems.setMaxHeight(Double.MAX_VALUE);

		getSkinnable().impl_getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		cbTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			if (n!=null) {
//				lvItems.getItems().clear();
//				cbTypes.getSelectionModel().select(0);
//			}
//		});

		cbTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				lvItems.getItems().clear();
				lvItems.getItems().addAll(
						rules.getListByType(CoriolisItemTemplate.class).stream().filter(item -> item.getType()==cbTypes.getValue()).collect(Collectors.toList()));
			}
		});

		lvItems.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			this.getSkinnable().setSelectedItem(n);
		});

		tfName.setOnAction(event -> {
			String filter = tfName.getText();
			List<CoriolisItemTemplate> poss = rules.getListByType(CoriolisItemTemplate.class);
			List<CoriolisItemTemplate> filtered = new ArrayList<>();
			if (filter.length()==0) {
				filtered = poss;
			} else {
				// Filter those names that match search string
				for (CoriolisItemTemplate tmp : poss) {
					if (tmp.getName().toLowerCase().contains(filter.toLowerCase())) {
						filtered.add(tmp);
					}
				}
			}
			lvItems.getItems().clear();
			lvItems.getItems().addAll(filtered);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			lvItems.getItems().clear();
			lvItems.getItems().addAll(rules.getListByType(CoriolisItemTemplate.class));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	SelectionModel<CoriolisItemTemplate> getSelectionModel() {
		return lvItems.getSelectionModel();
	}

}
