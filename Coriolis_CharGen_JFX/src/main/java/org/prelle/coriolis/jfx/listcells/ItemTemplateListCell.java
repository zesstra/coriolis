/**
 *
 */
package org.prelle.coriolis.jfx.listcells;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.yearzeroengine.ItemTemplate;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.charctrl.GearController;

/**
 * @author Stefan
 *
 */
public class ItemTemplateListCell extends ListCell<CoriolisItemTemplate> {

	private CharacterController<CoriolisCharacter> parent;
	private GearController control;

	private VBox layout;
	private Label lbName, lbRef, lbCost;

	//--------------------------------------------------------------------
	public ItemTemplateListCell(CharacterController<CoriolisCharacter> ctrl) {
		this.parent = ctrl;
		this.control = ctrl.getGearController();

		lbName = new Label(); lbName.getStyleClass().add("base");
		lbRef = new Label();
		lbCost = new Label();
		HBox bxSecLine = new HBox(5, lbRef, lbCost);

		layout = new VBox(5, lbName, bxSecLine);
		layout.setAlignment(Pos.TOP_LEFT);

		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CoriolisItemTemplate item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(layout);
			lbName.setText(item.getName());
			lbRef.setText(item.getProductNameShort()+" "+item.getPage()+", ");
			lbCost.setText(item.getCost().toString());
			if (item.getCost().getMin()>parent.getCharacter().getBirr()) {
				lbCost.setStyle("-fx-text-fill: red");
			} else {
				lbCost.setStyle("");
			}
			this.setDisable(!control.canBeSelected(item));
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		ItemTemplate data = itemProperty().get();
//		logger.debug("drag started for "+data);
		if (data==null)
			return;
//		logger.debug("canBeDeselected = "+parent.getItemTemplateController().canBeDeselected(data));
		if (!parent.getGearController().canBeSelected(data))
			return;

		Node source = (Node) event.getSource();
//		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);

        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        String id = "item:"+data.getId();
        content.putString(id);
        db.setContent(content);

        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);

        event.consume();
    }

}
