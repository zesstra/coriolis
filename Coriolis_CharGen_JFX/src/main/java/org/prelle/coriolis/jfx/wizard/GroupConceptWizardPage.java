/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.GroupConcept;
import org.prelle.coriolis.charctrl.CoriolisGroupController;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.yearzeroengine.Talent;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class GroupConceptWizardPage extends WizardPage {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private ChoiceBox<GroupConcept> cbConcept;
	private ChoiceBox<Talent> cbTalent;
	private TextField[] tfName;
	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	public GroupConceptWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.groupconcept1.title"));
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		cbConcept = new ChoiceBox<>();
		cbConcept.getItems().addAll(ctrl.getRuleset().getListByType(GroupConcept.class));
		cbConcept.setConverter(new StringConverter<GroupConcept>() {
			public String toString(GroupConcept value) { return value.getName();}
			public GroupConcept fromString(String arg0) { return null; }
		});

		cbTalent = new ChoiceBox<>();
		cbTalent.getItems().addAll(ctrl.getRuleset().getListByType(Talent.class).stream().filter(tal -> tal.getType()==Talent.Type.GROUP).collect(Collectors.toList()));
		cbTalent.setConverter(new StringConverter<Talent>() {
			public String toString(Talent value) { return value.getName();}
			public Talent fromString(String arg0) { return null; }
		});

		tfName = new TextField[6];
		for (int i=0; i<tfName.length; i++) {
			tfName[i] = new TextField();
			tfName[i].setPromptText(RES.getString("wizard.groupconcept1.otherchars.prompt"));
			tfName[i].setStyle("-fx-pref-width: 10em");
		}

		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.groupconcept1.intro"));
		lbIntro.setWrapText(true);

		/*
		 * Group concept
		 */
		Label hdConcept = new Label(RES.getString("label.group_concept"));
		hdConcept.getStyleClass().add("subtitle");
		Label lbConceptEx = new Label(RES.getString("label.group_concept.explain"));
		lbConceptEx.setWrapText(true);
		Label lbConcept = new Label(RES.getString("label.group_concept"));
		lbConcept.getStyleClass().add("base");

//		HBox bxConcept = new HBox(lbConcept, cbConcept);
//		bxConcept.setStyle("-fx-spacing: 1em");
//		bxConcept.setAlignment(Pos.CENTER_LEFT);

		/*
		 * Group talent
		 */
		Label lbTalentEx = new Label(RES.getString("label.group_talent.explain"));
		lbTalentEx.setWrapText(true);
		Label lbTalent = new Label(RES.getString("label.group_talent"));
		lbTalent.getStyleClass().add("base");

//		HBox bxTalent = new HBox(lbTalent, cbTalent);
//		bxTalent.setStyle("-fx-spacing: 1em");
//		bxTalent.setAlignment(Pos.CENTER_LEFT);

		GridPane conceptAndTalentGrid = new GridPane();
		conceptAndTalentGrid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 1em;");
		conceptAndTalentGrid.add(lbConceptEx, 0, 0, 2,1);
		conceptAndTalentGrid.add(lbConcept  , 0, 1);
		conceptAndTalentGrid.add(cbConcept  , 1, 1);
		conceptAndTalentGrid.add(lbTalentEx, 0, 2, 2,1);
		conceptAndTalentGrid.add(lbTalent  , 0, 3);
		conceptAndTalentGrid.add(cbTalent  , 1, 3);

		/*
		 * Other players / characters
		 */
		Label hdOthers = new Label(RES.getString("wizard.groupconcept1.otherchars.heading"));
		hdOthers.getStyleClass().add("subtitle");
		Label lbOthersEx = new Label(RES.getString("label.otherchars.explain"));
		lbOthersEx.setWrapText(true);

		GridPane grid = new GridPane();
		for (int i=0; i<tfName.length; i++) {
			Label lbName = new Label(RES.getString("label.character")+" "+(i+1));
			lbName.getStyleClass().add("base");
			grid.add(lbName, (i%2)*2, (i/2));
			grid.add(tfName[i], (i%2)*2+1, (i/2));
		}
		grid.setStyle("-fx-hgap: 0.5em; -fx-vgap: 0.6em");

		VBox content = new VBox(10);
		content.getChildren().addAll(lbIntro);
		content.getChildren().addAll(hdConcept, conceptAndTalentGrid);
//		content.getChildren().addAll(lbTalentEx, bxTalent);
		content.getChildren().addAll(hdOthers , lbOthersEx, grid);
		VBox.setMargin(hdConcept, new Insets(20, 0, 10, 0));
		VBox.setMargin(hdOthers, new Insets(20, 0, 10, 0));


		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, line, explain);
		content.setStyle("-fx-pref-width: 40em; -fx-min-width:25em");
		explain.setStyle("-fx-pref-width: 25em; -fx-min-width:15em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbConcept.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			((CoriolisGroupController)ctrl.getGroupController()).setGroupConcept(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				lbSelectDesc.setText(n.getHelpText());
			}
		});

		cbTalent.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			((CoriolisGroupController)ctrl.getGroupController()).setGroupTalent(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				lbSelectDesc.setText(n.getHelpText());
			}
		});
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageLeft(org.prelle.javafx.CloseType)
	 */
	@Override
	public void pageLeft() {
		updateCharNames();
	}

	//--------------------------------------------------------------------
	private void updateCharNames() {
		List<String> list = new ArrayList<>();
		for (TextField tmp : tfName) {
			if (tmp.getText()!=null && !tmp.getText().isEmpty())
				list.add(tmp.getText());
		}
		ctrl.getGroupController().setPCNames(list);
	}

}
