/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.CoriolisTools;
import org.prelle.coriolis.ItemType;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CarriedItemListCell;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.listcells.ItemTemplateListCell;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;
import org.prelle.yearzeroengine.modifications.ItemModification;
import org.prelle.yearzeroengine.modifications.ModificationChoice;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class GearWizardPage extends WizardPage implements
		GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;

	private VBox bxDecisions;

	private FreePointsNode lbStrengthLeft;
	private FreePointsNode lbBirrLeft;
	private ChoiceBox<ItemType> cbItemType;
	private ListView<CoriolisItemTemplate> lvAvailable;
	private ListView<CoriolisCarriedItem> lvSelected;

	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public GearWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.gear.title"));
		GenerationEventDispatcher.addListener(this);

		cbItemType.getSelectionModel().select(0);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		/*
		 * concept gear
		 */
		bxDecisions = new VBox();
		bxDecisions.setStyle("-fx-spacing: 0.5em");

		/*
		 * Additional gear
		 */
		cbItemType = new ChoiceBox<>();
		cbItemType.getItems().addAll(ItemType.values());
		cbItemType.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType val) {	return val.getName();}
			public ItemType fromString(String arg0) {return null;}
		});
		lvAvailable = new ListView<CoriolisItemTemplate>();
		lvAvailable.setCellFactory(lv -> new ItemTemplateListCell(ctrl));
		lvAvailable.setStyle("-fx-min-width: 21em");
		Label placeholder = new Label(RES.getString("wizard.gear.templates.avprompt"));
		placeholder.setWrapText(true);
		lvAvailable.setPlaceholder(placeholder);
		
		lvSelected  = new ListView<CoriolisCarriedItem>();
		lvSelected.getItems().addAll(ctrl.getCharacter().getItems());
		lvSelected.setCellFactory(lv -> new CarriedItemListCell(ctrl));
		lvSelected.setStyle("-fx-min-width: 21em");
		placeholder = new Label(RES.getString("wizard.gear.templates.selprompt"));
		placeholder.setWrapText(true);
		lvSelected.setPlaceholder(placeholder);

		lbStrengthLeft = new FreePointsNode();
		lbStrengthLeft.setName(RES.getString("label.load.short"));
		lbStrengthLeft.setStyle("-fx-font-size: +200%");
		lbBirrLeft = new FreePointsNode();
		lbBirrLeft.setName(RES.getString("label.birr.short"));
//		lbBirrLeft.setStyle("-fx-font-size: +200%");

		/*
		 * Explain
		 */
		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectName.setWrapText(true);
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		/*
		 * Concept gear
		 */
		Label hdConcept = new Label(RES.getString("wizard.gear.concept.heading"));
		hdConcept.getStyleClass().add("subtitle");
		Label lbIntroConcept = new Label(RES.getString("wizard.gear.concept.intro"));
		lbIntroConcept.setWrapText(true);

		/*
		 * Additional
		 */
		Label hdExtra = new Label(RES.getString("wizard.gear.extra.heading"));
		hdExtra.getStyleClass().add("subtitle");
		Label lbIntroExtra = new Label(RES.getString("wizard.gear.extra.intro"));
		lbIntroExtra.setWrapText(true);
		HBox bxIntroExtra = new HBox(10, lbStrengthLeft, lbBirrLeft, lbIntroExtra);
		VBox bxAvailable = new VBox(10, cbItemType, lvAvailable);
		bxAvailable.setStyle("-fx-spacing: 1em");
		HBox bxLists = new HBox(bxAvailable, lvSelected);
		bxLists.setStyle("-fx-spacing: 3em");

		VBox content = new VBox(5);
		content.getChildren().addAll(hdConcept, lbIntroConcept, bxDecisions);
		content.getChildren().addAll(hdExtra, bxIntroExtra, bxLists);
		VBox.setMargin(hdExtra, new Insets(20, 0, 0, 0));

		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, line, explain);
		allContent.setStyle("-fx-spacing: 2em");
//		content.setStyle("-fx-pref-width: 60em; -fx-min-width:35em");
//		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:15em");
		content.setStyle("-fx-pref-width: 50em; -fx-min-width:25em");
		explain.setStyle("-fx-pref-width: 15em; -fx-min-width:10em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbItemType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			lvAvailable.getItems().clear();
			lvAvailable.getItems().addAll(
					ctrl.getRuleset().getListByType(CoriolisItemTemplate.class)
					.stream()
					.filter(t -> t.getType()==n)
					.collect(Collectors.toList()));
		});

		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> updateHelp(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				updateHelp(n.getItem());
		});

		/*
		 * Drag & Drop
		 */
		lvSelected.setOnDragOver(ev -> dragOverSelected(ev));
		lvSelected.setOnDragDropped(ev -> dragDroppedSelected(ev));
		lvAvailable.setOnDragOver(ev -> dragOverAvailable(ev));
		lvAvailable.setOnDragDropped(ev -> dragDroppedAvailable(ev));
	}

	//--------------------------------------------------------------------
	private void updateHelp(YearZeroModule n) {
		lbSelectName.setText(n.getName());
		lbSelectRef.setText(n.getProductName()+" "+n.getPage());
		lbSelectDesc.setText(n.getHelpText());
	}

	//--------------------------------------------------------------------
	private void updateDecisions() {
		bxDecisions.getChildren().clear();

		List<DecisionToMake> decide = ctrl.getCharacterClassController().getDecisions();
		int count=1;
		for (DecisionToMake choose : decide) {
			Label lblNr = new Label(count+". ");
			lblNr.getStyleClass().add("base");
			
			HBox line = new HBox();
			line.setAlignment(Pos.CENTER_LEFT);
			line.setStyle("-fx-spacing: 1em");
			line.getChildren().add(lblNr);
			ToggleGroup group = new ToggleGroup();
			group.setUserData(choose);
			if (choose.getChoice()==null) {
				logger.error("No choice for concept "+ctrl.getCharacterClassController().getSelected());
				return;
			}
			/*
			 * Build toggle buttons for each option
			 */
			boolean hasItemMod = false;
			ModificationChoice choice = (ModificationChoice) choose.getChoice();
			for (Modification mod : choice.getOptionList()) {
				if (hasItemMod) {
					line.getChildren().add(new Label(" "+RES.getString("label.or")+" "));
				}
				
				if (mod instanceof ItemModification)
					hasItemMod = true;
				String name = CoriolisTools.toString(mod);
				ToggleButton btn = new ToggleButton(name);
				btn.setToggleGroup(group);
				btn.setUserData(mod);
				line.getChildren().add(btn);
				// Eventually set
				boolean selected = (choose.getDecision()!=null)?choose.getDecision().contains(mod):false;
//				logger.debug("********** is option "+mod+" decided = "+selected);
				btn.setSelected(selected);
			}
			// Interactivity
			group.selectedToggleProperty().addListener( (ov,o,n) -> {
				if (n==null)
					return;
				Modification choosen = (Modification)n.getUserData();
				DecisionToMake dec = (DecisionToMake)n.getToggleGroup().getUserData();
				ctrl.getCharacterClassController().decide(dec, choosen);
				if (choosen instanceof TalentModification) {
					updateHelp( ((TalentModification)choosen).getTalent());
				} else if (choosen instanceof ItemModification) {
					updateHelp( ((ItemModification)choosen).getItem());
				} else
					logger.warn("Don't know how to show help for "+choosen.getClass());
			});
			// Accept only those with at least one ItemModificiation
			if (hasItemMod) {
				bxDecisions.getChildren().add(line);
				count++;
			}
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			lvSelected.getItems().clear();
			lvSelected.getItems().addAll(ctrl.getCharacter().getItems());
			lbStrengthLeft.setPoints(ctrl.getGearController().getEncumbrancePointsLeft());
			lbBirrLeft.setPoints(ctrl.getCharacter().getBirr());
			updateDecisions();
			lvAvailable.refresh();
			break;
		}

	}

	//-------------------------------------------------------------------
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
	        if (event.getDragboard().getString().startsWith("item:"))
            /* allow for both copying and moving, whatever user chooses */
	        	event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

	//-------------------------------------------------------------------
	private void dragDroppedSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	logger.debug("Dropped "+enhanceID);
        	// Get reference for ID
        	if (enhanceID.startsWith("item:")) {
        		StringTokenizer tok = new StringTokenizer(enhanceID.substring(5),"|");
        		String talID = tok.nextToken();
//        		String descr = tok.nextToken();
//        		String idref = tok.nextToken();
//        		if (descr!=null) descr=descr.substring(5);
//        		if (idref!=null) idref=idref.substring(6);
//        		if ("null".equals(descr)) descr=null;
//        		if ("null".equals(idref)) idref=null;

        		CoriolisItemTemplate toMove = ctrl.getRuleset().getByType(CoriolisItemTemplate.class, talID);
        		logger.debug("Select item "+toMove);
        		CarriedItem<CoriolisItemTemplate> selected = ctrl.getGearController().select(toMove, ItemLocation.BODY);
        		if (selected!=null) {
        			logger.info("Selected item "+toMove);
        		} else {
        			logger.warn("Failed selecting item: "+toMove);
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
	        if (event.getDragboard().getString().startsWith("carried:"))
            /* allow for both copying and moving, whatever user chooses */
	        	event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

	//-------------------------------------------------------------------
	private void dragDroppedAvailable(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	logger.debug("Dropped "+enhanceID);
        	// Get reference for ID
        	if (enhanceID.startsWith("carried:")) {
        		StringTokenizer tok = new StringTokenizer(enhanceID.substring(8),"|");
        		String talID = tok.nextToken();
        		UUID uniqID = UUID.fromString(tok.nextToken());

        		CoriolisCarriedItem toMove = ctrl.getCharacter().getItem(uniqID);
        		if (toMove==null) {
        			logger.warn("Trying to drag item "+enhanceID+" which does not exist in character");
        	        event.consume();
        			return;
        		}
        		logger.debug("Deselect item "+toMove);
        		if (!ctrl.getGearController().canBeDeselected(toMove)) {
        			logger.warn("Trying to drag/deselect item that can not be deselected: "+toMove);
        	        event.consume();
        			return;
        		}
        		ctrl.getGearController().deselect(toMove);
       			logger.info("Deselected item "+toMove);
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

}
