/**
 *
 */
package org.prelle.coriolis.jfx.listcells;

import javafx.scene.Node;

import org.prelle.yearzeroengine.BasePluginData;

/**
 * @author Stefan
 *
 */
public interface BasePluginDataCell <T> {

	public Node getGraphic();

	public void updateWithItem(T item);

}
