package org.prelle.coriolis.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.ExpLine;
import org.prelle.coriolis.jfx.sections.GearSection;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.yearzeroengine.BasePluginData;
import org.prelle.yearzeroengine.ItemLocation;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class CoriolisGearPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(CoriolisGearPage.class.getName());

	private CharacterHandle handle;
	private CoriolisCharacterController control;
	private ScreenManagerProvider provider;

	private ExpLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private GearSection  body;
	private GearSection  cabin;
	private Section line1;

	//-------------------------------------------------------------------
	public CoriolisGearPage(CoriolisCharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("coriolis-gear");
		this.control = control;
		this.handle  = handle;
		this.provider = provider;

		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new ExpLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		body = new GearSection(UI.getString("section.body"), control, provider, ItemLocation.BODY);
		cabin= new GearSection(UI.getString("section.cabin"), control, provider, ItemLocation.CABIN);

		line1 = new DoubleSection(body, cabin);
		getSectionList().add(line1);

		// Interactivity
		body.showHelpForProperty().addListener( (ov,o,n) -> updateHelp((n!=null)?n.getItem():null));
		cabin  .showHelpForProperty().addListener( (ov,o,n) -> updateHelp((n!=null)?n.getItem():null));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(UI.getString("label.carryitems.short"));

		initLine1();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getGearController().getEncumbrancePointsLeft());
		firstLine.setData(control.getCharacter());
		line1.getToDoList().setAll(control.getGearController().getToDos());

		body.refresh();
		cabin.refresh();
	}

}
