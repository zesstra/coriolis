/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisSkillCategory;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.tables.SkillValueTableView;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class SkillsWizardPage extends WizardPage implements GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private FreePointsNode lbPointsLeft;
	private SkillValueTableView tbGeneric;
	private SkillValueTableView tbAdvanced;

	private DescriptionPane explain;

	//--------------------------------------------------------------------
	public SkillsWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.skills.title"));
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lbPointsLeft = new FreePointsNode();
		lbPointsLeft.setName(RES.getString("label.points.short"));
		lbPointsLeft.setStyle("-fx-font-size: +200%");
		tbGeneric = new SkillValueTableView(ctrl, CoriolisSkillCategory.GENERAL);
//		tbGeneric.getItems().retainAll(ctrl.getCharacter().getSkills(ctrl.getRuleset().getSkillsByCategory(CoriolisSkillCategory.GENERAL)));
		tbAdvanced= new SkillValueTableView(ctrl, CoriolisSkillCategory.ADVANCED);
//		tbAdvanced.getItems().retainAll(ctrl.getCharacter().getSkills(ctrl.getRuleset().getSkillsByCategory(CoriolisSkillCategory.ADVANCED)));

		explain = new DescriptionPane();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.skills.intro"));
		lbIntro.setWrapText(true);

		HBox line = new HBox();
		line.getChildren().addAll(lbPointsLeft, lbIntro);
		line.setAlignment(Pos.TOP_LEFT);
		line.setStyle("-fx-spacing: 2em;");

		HBox skills = new HBox(tbGeneric, tbAdvanced);
		skills.setStyle("-fx-spacing: 2em;");
		tbGeneric.setStyle("-fx-max-height: 22.3em;");
		tbAdvanced.setStyle("-fx-max-height: 22.3em;");

		VBox content = new VBox(line, skills);
		line.setStyle("-fx-spacing: 2em;");



		Separator sep = new Separator(Orientation.VERTICAL);
		sep.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, sep, explain);
		allContent.setId("wizard-skills");
		allContent.setStyle("-fx-spacing: 1em");
//		content.setStyle("-fx-pref-width: 60em; -fx-min-width:35em; -fx-spacing: 2em");
//		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:15em");
		content.setStyle("-fx-pref-width: 55em; -fx-min-width:35em; -fx-spacing: 2em");
		explain.setStyle("-fx-pref-width: 15em; -fx-min-width:10em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void updateHelp(YearZeroModule n) {
		explain.setText(n.getName(), n.getProductName()+" "+n.getPage(), n.getHelpText());
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		tbGeneric.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> updateHelp(n.getModifyable()));
		tbAdvanced.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> updateHelp(n.getModifyable()));
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is shown to user
	 */
	public void pageVisited() {
		tbGeneric.refresh();
		tbAdvanced.refresh();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			int points = ctrl.getSkillController().getPointsToSpend();
			lbPointsLeft.setPoints(points);
			tbGeneric.refresh();
			tbAdvanced.refresh();
			break;
		default:
		}

	}
}

