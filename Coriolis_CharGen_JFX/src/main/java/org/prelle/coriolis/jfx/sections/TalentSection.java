package org.prelle.coriolis.jfx.sections;

import java.util.Arrays;
import java.util.Collection;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.SelectorWithHelp;
import org.prelle.coriolis.jfx.dialogs.TalentSelector;
import org.prelle.coriolis.jfx.listcells.TalentValueListCell;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.charctrl.TalentController;

import javafx.scene.control.Label;

/**
 * @author Stefan Prelle
 *
 */
public class TalentSection extends GenericListSection<TalentValue> {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(TalentSection.class.getName());

	private Collection<Talent.Type> allowed;

	//-------------------------------------------------------------------
	public TalentSection(String title, CoriolisCharacterController ctrl, ScreenManagerProvider provider, Talent.Type... type) {
		super(title, ctrl, provider);
		this.allowed = Arrays.asList(type);
		initPlaceholder();
		initCellFactories();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(UI.getString("talentlistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//--------------------------------------------------------------------
	protected  void initCellFactories() {
		list.setCellFactory(lv -> new TalentValueListCell(control));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		list.setStyle("-fx-min-width: 22em; -fx-pref-width: 29em; -fx-pref-height: 40em");
		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getTalents().stream().filter(ci -> allowed.contains(ci.getTalent().getType())).collect(Collectors.toList()));
		
		getAddButton().setDisable(control.getCharacter().getExpFree()<5);
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.debug("opening talent selection dialog");
		
		Talent.Type[] allow = new Talent.Type[allowed.size()];
		allow = allowed.toArray(allow);
		TalentSelector selector = new TalentSelector(allow, control);
		SelectorWithHelp<Talent> pane = new SelectorWithHelp<Talent>(selector);
		ManagedDialog dialog = new ManagedDialog(UI.getString("selectiondialog.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.debug("Closed with "+close);
		if (close==CloseType.OK) {
			Talent toMove = selector.getSelectedItem();
			logger.debug("Selected talent: "+toMove);
			if (toMove!=null) {
        		// Now select
         		boolean selected = ((TalentController)control.getTalentController()).select(toMove);
        		if (selected) {
        			logger.info("Added talent "+toMove);
        		} else {
        			logger.warn("Failed adding talent: "+toMove);
        		}        			

//        		CarriedItem<ItemTemplate> item = control.getTalentController().select(selected, location);
//				if (item==null) {
//					getManagerProvider().getScreenManager().showAlertAndCall(AlertType.ERROR, "header", "Cannot select "+selected);
//				}
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	protected void onDelete() {
		TalentValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("TalentValue to deselect: "+selected);
		control.getTalentController().deselect(selected);
		refresh();
	}

}
