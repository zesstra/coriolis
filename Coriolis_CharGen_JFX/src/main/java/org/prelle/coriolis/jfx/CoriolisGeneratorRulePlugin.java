/**
 *
 */
package org.prelle.coriolis.jfx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.charlvl.CoriolisCharacterLeveller;
import org.prelle.javafx.ScreenManager;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class CoriolisGeneratorRulePlugin implements RulePlugin<CoriolisCharacter>, CommandBusListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;
	private static PropertyResourceBundle HELP = CoriolisCharGenJFXConstants.HELP;

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	//-------------------------------------------------------------------
	static {	
		FEATURES.add(RulePluginFeatures.CHARACTER_CREATION);
//		FEATURES.add(RulePluginFeatures.DATA_INPUT);
	}

	private ConfigContainer cfgCoriolis;

	//-------------------------------------------------------------------
	/**
	 */
	public CoriolisGeneratorRulePlugin() {
//		NewPriorityCharacterGenerator charGen = new NewPriorityCharacterGenerator();
//		charGen.setResourceBundle(UI);
//		charGen.setHelpResourceBundle(HELP);
//		charGen.setPlugin(this);
//		CharacterGeneratorRegistry.register(charGen);

//		DirectInputCharacterGenerator input = new DirectInputCharacterGenerator();
//		input.setResourceBundle(UI);
//		input.setHelpResourceBundle(HELP);
//		input.setPlugin(this);
//		CharacterGeneratorRegistry.register(input);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CHARGEN";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Coriolis Character Generator";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.CORIOLIS;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		List<RulePluginFeatures> ret = new ArrayList<>(FEATURES);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		logger.info("attachConfigurationTree below "+addBelow);
		try {
			cfgCoriolis = (ConfigContainer) addBelow.getChild("coriolis");
		} catch (NoSuchElementException e) {
			logger.debug("Coriolis node does not exist yet - add it");
			cfgCoriolis = addBelow.createContainer("coriolis");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			if (values[0]!=RoleplayingSystem.CORIOLIS) return false;
			if (!(values[1] instanceof CoriolisCharacter)) return false;
			if (!(values[2] instanceof CharacterHandle)) return false;
			if (!(values[4] instanceof ScreenManager)) return false;
			return true;
		case SHOW_CHARACTER_CREATION_GUI:
			if (values[0]!=RoleplayingSystem.CORIOLIS) return false;
			if (!(values[2] instanceof ScreenManager)) return false;
			return true;
//		case SHOW_DATA_INPUT_GUI:
//			if (values[0]!=RoleplayingSystem.CORIOLIS) return false;
//			if (!(values[2] instanceof ScreenManager)) return false;
//			return true;
		default:
			return false;
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		logger.debug("Handle "+type);
		if (!willProcessCommand(src, type, values))
			return new CommandResult(type, false, null, false);

		logger.debug("Handle "+type);
		ScreenManager manager;
		CoriolisCharacterController control;
		CoriolisCharacter model;
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			logger.debug("start character modification");
			model = (CoriolisCharacter)values[1];
			control = new CoriolisCharacterLeveller(model);
			CharacterHandle handle = (CharacterHandle)values[2];
			manager = (ScreenManager)values[4];

			CharacterViewScreenCoriolis screen;
			try {
				screen = new CharacterViewScreenCoriolis(control, handle);
				manager.navigateTo(screen);
//				manager.show(screen, "css/shadowrun-common.css", "css/shadowrun-dark.css");
			} catch (Exception e) {
				logger.error("Failed opening character screen",e);
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE,2, e.toString());
			}

			return new CommandResult(type, true);
		case SHOW_CHARACTER_CREATION_GUI:
			logger.info("start character creation");
			control = new CoriolisCharacterGenerator();
			((CoriolisCharacterGenerator)control).start();
			manager = (ScreenManager)values[2];

			screen = new CharacterViewScreenCoriolis(control, null);
			manager.navigateTo(screen);

//			logger.debug("You could give a warning here");
//			manager.showAlertAndCall(AlertType.NOTIFICATION, "Achtung!", "Zwar werden 'Schattenläufer' und 'Straßengrimoire' unterstützt, aber Metagenetik, Initiation sowie Erschaffung außer Prioriäten- und Karmasystem noch nicht - das kommt erst in späteren Versionen.\n\n"+
//			"Wer Fehler in bestehenden Funktionen findet, kann uns dies melden. Siehe hierzu: http://www.rpgframework.de/index.php/de/support/fehler-melden/\n"+
//			"Wer Java-Entwickler ist und mitmachen möchte, kann uns an  genesis@rpgframework.de  eine Mail schreiben.\n"
//			);

			handle = screen.startGeneration();
			if (handle!=null) {
				manager.closeScreen();
				control = new CoriolisCharacterLeveller(control.getCharacter());
				screen = new CharacterViewScreenCoriolis(control, null);
				manager.navigateTo(screen);
			}

			CommandResult result = new CommandResult(type, true);
			result.setReturnValue(control.getCharacter());
			return result;
		default:
			return new CommandResult(type, false);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		CommandBus.registerBusCommandListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return getClass().getResourceAsStream("i18n/coriolis/chargenui.html");
	}

	//-------------------------------------------------------------------
	public List<String> getLanguages() {
		return Arrays.asList(Locale.ENGLISH.getLanguage(), Locale.GERMAN.getLanguage());
	}

}
