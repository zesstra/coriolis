package org.prelle.coriolis.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.ExpLine;
import org.prelle.coriolis.jfx.sections.GearSection;
import org.prelle.coriolis.jfx.sections.TalentSection;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.yearzeroengine.BasePluginData;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.Talent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class CoriolisTalentPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(CoriolisTalentPage.class.getName());

	private CharacterHandle handle;
	private CoriolisCharacterController control;
	private ScreenManagerProvider provider;

	private ExpLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private TalentSection  body;
	private TalentSection  cabin;
	private Section line1;

	//-------------------------------------------------------------------
	public CoriolisTalentPage(CoriolisCharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("coriolis-talents");
		this.control = control;
		this.handle  = handle;
		this.provider = provider;

		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new ExpLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		body = new TalentSection(UI.getString("section.normal"), control, provider, Talent.Type.GENERAL, Talent.Type.BIONIC, Talent.Type.CYBERNETIC, Talent.Type.MYSTICAL);
		cabin= new TalentSection(UI.getString("section.fixed"), control, provider, Talent.Type.GROUP, Talent.Type.HUMANITE, Talent.Type.MYSTICAL);

		line1 = new DoubleSection(body, cabin);
		getSectionList().add(line1);

		// Interactivity
		body.showHelpForProperty().addListener( (ov,o,n) -> updateHelp((n!=null)?n.getTalent():null));
		cabin  .showHelpForProperty().addListener( (ov,o,n) -> updateHelp((n!=null)?n.getTalent():null));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initLine1();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getCharacter().getExpFree());
		firstLine.setData(control.getCharacter());
		line1.getToDoList().setAll(control.getTalentController().getToDos());

		body.refresh();
		cabin.refresh();
	}

}
