package org.prelle.coriolis;

public enum SelectOption {
	WEIGHT,
	TIER,
	COST,
	COUNT,
	VAL,
	SKILL,
	NAME
}