/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 * @author Stefan
 *
 */
public class WeaponData {

	public enum Range {
		CLOSE,
		SHORT,
		LONG,
		EXTREME;

		//-------------------------------------------------------------------
		public String getName() {
			return CoriolisCore.getI18nResources().getString("range."+this.name().toLowerCase());
		}

		public String getShortName() {
			return CoriolisCore.getI18nResources().getString("range."+this.name().toLowerCase()+ ".short");
		}

	}

	@Attribute
	private int init;
	@Attribute
	private int damage;
	@Attribute
	private int crit;
	@Attribute(name="bp")
	private int blastPower;
	@Attribute
	private Range range;
	@ElementList(type=FeatureReference.class,entry="feature",inline=true)
	private ArrayList<FeatureReference> features;

	//--------------------------------------------------------------------
	/**
	 */
	public WeaponData() {
		features = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the init
	 */
	public int getInitiative() {
		return init;
	}

	//--------------------------------------------------------------------
	/**
	 * @param init the init to set
	 */
	public void setInitiative(int init) {
		this.init = init;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public int getDamage() {
		return damage;
	}

	//--------------------------------------------------------------------
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(int damage) {
		this.damage = damage;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the crit
	 */
	public int getCrit() {
		return crit;
	}

	//--------------------------------------------------------------------
	/**
	 * @param crit the crit to set
	 */
	public void setCrit(int crit) {
		this.crit = crit;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the blastPower
	 */
	public int getBlastPower() {
		return blastPower;
	}

	//--------------------------------------------------------------------
	/**
	 * @param blastPower the blastPower to set
	 */
	public void setBlastPower(int blastPower) {
		this.blastPower = blastPower;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the features
	 */
	public List<FeatureReference> getFeatures() {
		return features;
	}

	//--------------------------------------------------------------------
	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<FeatureReference> features) {
		this.features = new ArrayList<FeatureReference>(features);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the range
	 */
	public Range getRange() {
		return range;
	}

	//--------------------------------------------------------------------
	/**
	 * @param range the range to set
	 */
	public void setRange(Range range) {
		this.range = range;
	}

}
