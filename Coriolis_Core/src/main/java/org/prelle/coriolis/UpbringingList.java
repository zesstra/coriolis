/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="upbringings")
@ElementList(entry="upbringing",type=Upbringing.class,inline=true)
public class UpbringingList extends ArrayList<Upbringing> {

}
