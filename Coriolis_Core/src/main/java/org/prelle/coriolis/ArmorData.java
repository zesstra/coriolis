/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 * @author Stefan
 *
 */
public class ArmorData {

	@Attribute
	private int rating;
	@Attribute(name="extra")
	private int extraFeatures;
	@ElementList(type=FeatureReference.class,entry="feature",inline=true)
	private ArrayList<FeatureReference> features;

	//--------------------------------------------------------------------
	public ArmorData() {
		features = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the features
	 */
	public List<FeatureReference> getFeatures() {
		return features;
	}

	//--------------------------------------------------------------------
	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<FeatureReference> features) {
		this.features = new ArrayList<FeatureReference>(features);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	//--------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the extraFeatures
	 */
	public int getExtraFeatures() {
		return extraFeatures;
	}

	//--------------------------------------------------------------------
	/**
	 * @param extraFeatures the extraFeatures to set
	 */
	public void setExtraFeatures(int extraFeatures) {
		this.extraFeatures = extraFeatures;
	}

}
