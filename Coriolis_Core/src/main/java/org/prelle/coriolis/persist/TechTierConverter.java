package org.prelle.coriolis.persist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prelle.coriolis.TechTier;
import org.prelle.simplepersist.StringValueConverter;

public class TechTierConverter implements StringValueConverter<List<TechTier>> {

	//-------------------------------------------------------------------
	private static TechTier getType(String value) {
		if ("p".equalsIgnoreCase(value))
			return TechTier.P;
		if ("o".equalsIgnoreCase(value))
			return TechTier.O;
		if ("a".equalsIgnoreCase(value))
			return TechTier.A;
		throw new NoSuchElementException("Cannot convert '"+value+"' to TechTier");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public List<TechTier> read(String v) throws Exception {
		v = v.trim();
		List<TechTier> ret = new ArrayList<TechTier>();
		
		StringTokenizer tok = new StringTokenizer(v, " /");
		while (tok.hasMoreTokens()) {
			String val = tok.nextToken();
			TechTier type = null;
				try {
					type = TechTier.valueOf(val);
				} catch (NoSuchElementException e) {
					type = getType(val);
				}
			ret.add(type);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(List<TechTier> v) throws Exception {
		if (v.isEmpty())
			return null;
		
		StringBuffer buf = new StringBuffer();
		for (Iterator<TechTier> it = v.iterator(); it.hasNext(); ) {
			buf.append(it.next().toString());
			if (it.hasNext())
				buf.append("/");
		}
		
		return buf.toString();
	}
	
}