/**
 *
 */
package org.prelle.coriolis.persist;

import org.prelle.coriolis.HomeSystem;
import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({YearZeroEngineCore.KEY_RULES})
public class HomeSystemConverter implements StringValueConverter<HomeSystem> {

	private YearZeroEngineRuleset ruleset;

	//-------------------------------------------------------------------
	public HomeSystemConverter(RoleplayingSystem rules) {
		ruleset = YearZeroEngineCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in YearZeroEngineCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(HomeSystem value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public HomeSystem read(String idref) throws Exception {
		HomeSystem skill = ruleset.getByType(HomeSystem.class, idref);
		if (skill==null)
			throw new SerializationException("Unknown homesystem ID '"+idref);

		return skill;
	}

}
