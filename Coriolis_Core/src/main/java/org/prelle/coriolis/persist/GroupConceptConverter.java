/**
 *
 */
package org.prelle.coriolis.persist;

import org.prelle.coriolis.GroupConcept;
import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({YearZeroEngineCore.KEY_RULES})
public class GroupConceptConverter implements StringValueConverter<GroupConcept> {

	private YearZeroEngineRuleset ruleset;

	//-------------------------------------------------------------------
	public GroupConceptConverter(RoleplayingSystem rules) {
		ruleset = YearZeroEngineCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in YearZeroEngineCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(GroupConcept value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public GroupConcept read(String idref) throws Exception {
		GroupConcept skill = ruleset.getByType(GroupConcept.class, idref);
		if (skill==null)
			throw new SerializationException("Unknown groupconcept ID '"+idref);

		return skill;
	}

}
