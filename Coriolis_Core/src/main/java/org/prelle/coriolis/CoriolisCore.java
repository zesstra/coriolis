/**
 *
 */
package org.prelle.coriolis;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charproc.CalculateDerivedAttributes;
import org.prelle.coriolis.persist.CoriolisSkillCategoryConverter;
import org.prelle.simplepersist.Persister;
import org.prelle.yearzeroengine.BasePluginData;
import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.CharacterClassList;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.YearZeroTools;
import org.prelle.yearzeroengine.charproc.ApplyTalentModifications;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.character.DecodeEncodeException;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CoriolisCore extends YearZeroEngineCore {

	private static boolean alreadyInitialized = false;
	private static PropertyResourceBundle i18NResources;
	private static PropertyResourceBundle i18NHelpResources;
	private static YearZeroEngineRuleset ruleset;

	//-------------------------------------------------------------------
	static {
		i18NResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/coriolis/i18n/core");
		i18NHelpResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/coriolis/i18n/core-help");
		logger = CoriolisConstants.LOGGER;
	}

	//-------------------------------------------------------------------
	public static Logger getLogger() {
		return logger;
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nResources() {
		return i18NResources;
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nHelpResources() {
		return i18NHelpResources;
	}

	//-------------------------------------------------------------------
	public static YearZeroEngineRuleset getRuleset() {
		return ruleset;
	}

	//-------------------------------------------------------------------
	public static void initialize(RulePlugin<CoriolisCharacter> plugin) {
		if (alreadyInitialized)
			return;
		ruleset = YearZeroEngineCore.createRuleset(RoleplayingSystem.CORIOLIS, i18NResources, i18NHelpResources);
		ruleset.setSkillCategoryConverter(new CoriolisSkillCategoryConverter(RoleplayingSystem.CORIOLIS));
		Persister.putContext(YearZeroEngineCore.ITEM_TEMPLATE_CLASS, CoriolisItemTemplate.class);
		for (AttributeCoriolis att : AttributeCoriolis.values())
			ruleset.addAttribute(att);

		Class<CoriolisCore> clazz = CoriolisCore.class;
		loadUpbringings(ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/upbringings.xml"), i18NResources, i18NHelpResources);
		loadSkills     (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/skills.xml"), i18NResources, i18NHelpResources);
		loadTalents    (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/talents.xml"), i18NResources, i18NHelpResources);
		loadHumanTypes (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/humantypes.xml"), i18NResources, i18NHelpResources);
		loadHomeSystems(ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/homesystems.xml"), i18NResources, i18NHelpResources);
		loadIcons      (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/icons.xml"), i18NResources, i18NHelpResources);
		loadFeatures   (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/features.xml"), i18NResources, i18NHelpResources);
		loadItems      (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/items.xml"), i18NResources, i18NHelpResources, CoriolisItemTemplate.class, CoriolisItemTemplateList.class);
		loadCharacterClasses(ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/concepts.xml"), i18NResources, i18NHelpResources);
//		BasePluginData.flushMissingKeys();
//		System.exit(0);
		loadSubConcepts  (ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/subconcepts.xml"), i18NResources, i18NHelpResources);
		loadGroupConcepts(ruleset, plugin, clazz.getResourceAsStream("data/coriolis/core/groupconcepts.xml"), i18NResources, i18NHelpResources);

//		ruleset.getTemplate("promising").setDefaultChoice(true);
		BasePluginData.flushMissingKeys();
//		logger.fatal("STOP HERE");
//		System.exit(0);

		alreadyInitialized = true;
	}

	//-------------------------------------------------------------------
	public static void loadUpbringings(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load upbringings (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			UpbringingList list = ruleset.getSerializer().read(UpbringingList.class, in);
			logger.info("Successfully loaded "+list.size()+" character classes");

			// Set translation
			for (Upbringing tmp : list) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				ruleset.addByType(tmp);
				YearZeroTools.setModificationSource(tmp, tmp);
			}

			if (logger.isDebugEnabled()) {
				for (Upbringing tmp : list)
					logger.debug("* "+tmp.getName());
			}

		} catch (Exception e) {
			logger.fatal("Failed deserializing templates",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static Upbringing getUpbringing(String id) {
		return ruleset.getByType(Upbringing.class, id);
	}

	//-------------------------------------------------------------------
	public static void loadHumanTypes(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load human/humanites (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			HumanTypeList list = ruleset.getSerializer().read(HumanTypeList.class, in);
			logger.info("Successfully loaded "+list.size()+" human/humanites");

			for (HumanType tmp : list) {
				// Set translation
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				ruleset.addByType(tmp);
				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);
			}


			if (logger.isDebugEnabled()) {
				for (HumanType tmp : list)
					logger.debug("* "+tmp.getName());
			}

		} catch (Exception e) {
			logger.fatal("Failed deserializing human/humanites",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static HumanType getHumanType(String id) {
		return ruleset.getByType(HumanType.class, id);
	}

	//-------------------------------------------------------------------
	public static CharacterClassList loadCharacterClasses(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		CharacterClassList loaded = YearZeroEngineCore.loadCharacterClasses(ruleset, plugin, in, resources, helpResources);
		if (loaded!=null) {
			for (CharacterClass clazz : loaded) {

				// Load appearance face
				List<String> appearFace = new ArrayList<>();
				for (int i=0; i<10; i++) {
					String key = "characterclass."+clazz.getId()+".appearance.face"+i;
					if (resources.containsKey(key)) {
						appearFace.add(resources.getString(key));
					} else
						break;
				}
				clazz.setFluffOptions("appearance.face", appearFace);
//				logger.trace("  Appearance Face: add "+appearFace.size()+" options");

				// Load appearance clothing
				List<String> appearCloth = new ArrayList<>();
				for (int i=0; i<10; i++) {
					String key = "characterclass."+clazz.getId()+".appearance.clothing"+i;
					if (resources.containsKey(key)) {
						appearCloth.add(resources.getString(key));
					} else
						break;
				}
				clazz.setFluffOptions("appearance.clothing", appearCloth);
//				logger.trace("  Appearance Clothing: add "+appearFace.size()+" options");

				// Load problems
				List<String> problem = new ArrayList<>();
				for (int i=0; i<10; i++) {
					String key = "characterclass."+clazz.getId()+".problem"+i;
					if (resources.containsKey(key)) {
						problem.add(resources.getString(key));
					} else
						break;
				}
				clazz.setFluffOptions("problem", problem);
//				logger.trace("  Personal Problem: add "+appearFace.size()+" options");

				// Load relationships to PCs
				List<String> relatePC = new ArrayList<>();
				for (int i=0; i<10; i++) {
					String key = "characterclass."+clazz.getId()+".relatePC"+i;
					if (resources.containsKey(key)) {
						relatePC.add(resources.getString(key));
					} else
						break;
				}
				clazz.setFluffOptions("relatePC", relatePC);
//				logger.info("  Relationships to PCs: add "+relatePC.size()+" options");
			}
		}

		return loaded;
	}

	//-------------------------------------------------------------------
	public static CharacterClass getCharacterClass(String id) {
		return ruleset.getCharacterClass(id);
	}

	//-------------------------------------------------------------------
	public static void loadSubConcepts(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load subconcepts (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			SubConceptList list = ruleset.getSerializer().read(SubConceptList.class, in);
			logger.info("Successfully loaded "+list.size()+" subconcepts");

			for (SubConcept tmp : list) {
				// Set translation
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				ruleset.addByType(tmp);
				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);
			}


			if (logger.isDebugEnabled()) {
				for (SubConcept tmp : list)
					logger.debug("* "+tmp.getName());
			}

		} catch (Exception e) {
			logger.fatal("Failed deserializing templates",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static SubConcept getSubConcept(String id) {
		return ruleset.getByType(SubConcept.class, id);
	}

	//-------------------------------------------------------------------
	public static void loadGroupConcepts(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load group concepts (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			GroupConceptList list = ruleset.getSerializer().read(GroupConceptList.class, in);
			logger.info("Successfully loaded "+list.size()+" groupconcepts");

			for (GroupConcept tmp : list) {
				// Set translation
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				ruleset.addByType(tmp);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				// Patrons/Nemesises
				for (PatronOrNemesis patron : tmp.getPatronsAndNemesises()) {
					patron.setHelpResourceBundle(helpResources);
					patron.setResourceBundle(resources);
					patron.setPlugin(plugin);
					patron.getName();
					patron.getOrganization();
					patron.getDescription();
				}
				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);
			}

			if (logger.isDebugEnabled()) {
				for (GroupConcept tmp : list)
					logger.debug("* "+tmp.getName());
			}

			BasePluginData.flushMissingKeys();
//			logger.fatal("Stop here");
//			System.exit(0);
		} catch (Exception e) {
			logger.fatal("Failed deserializing templates",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static GroupConcept getGroupConcept(String id) {
		return ruleset.getByType(GroupConcept.class, id);
	}

	//-------------------------------------------------------------------
	public static void loadIcons(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load icons (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			IconList list = ruleset.getSerializer().read(IconList.class, in);
			logger.info("Successfully loaded "+list.size()+" icons");

			// Set translation
			for (Icon tmp : list) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				ruleset.addByType(tmp);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);
			}

			if (logger.isDebugEnabled()) {
				for (Icon tmp : list)
					logger.debug("* "+tmp.getName());
			}
			BasePluginData.flushMissingKeys();

		} catch (Exception e) {
			logger.fatal("Failed deserializing icons",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static Icon getIcon(String id) {
		return ruleset.getByType(Icon.class, id);
	}

	//-------------------------------------------------------------------
	public static void loadHomeSystems(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load homesystems (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			HomeSystemList list = ruleset.getSerializer().read(HomeSystemList.class, in);
			logger.info("Successfully loaded "+list.size()+" home systems");

			// Set translation
			for (HomeSystem tmp : list) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				ruleset.addByType(tmp);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
			}

			if (logger.isDebugEnabled()) {
				for (HomeSystem tmp : list)
					logger.debug("* "+tmp.getName());
			}
			BasePluginData.flushMissingKeys();

		} catch (Exception e) {
			logger.fatal("Failed deserializing homesystems",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static HomeSystem getHomeSystem(String id) {
		return ruleset.getByType(HomeSystem.class, id);
	}

	//-------------------------------------------------------------------
	public static CoriolisItemTemplate getItem(String id) {
		return ruleset.getByType(CoriolisItemTemplate.class, id);
	}
	//-------------------------------------------------------------------
	public static void loadFeatures(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load features (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			FeatureList list = ruleset.getSerializer().read(FeatureList.class, in);
			logger.info("Successfully loaded "+list.size()+" features");

			// Set translation
			for (Feature tmp : list) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				ruleset.addByType(tmp);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
			}

			if (logger.isDebugEnabled()) {
				for (Feature tmp : list)
					logger.debug("* "+tmp.getName());
			}
			BasePluginData.flushMissingKeys();

		} catch (Exception e) {
			logger.fatal("Failed deserializing features",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static Feature getFeature(String id) {
		return ruleset.getByType(Feature.class, id);
	}


	//-------------------------------------------------------------------
	public static byte[] save(CoriolisCharacter character) throws DecodeEncodeException {

		try {
//			logger.trace("Save "+character.dump());

			// Write to System.out
			StringWriter out = new StringWriter();
			ruleset.getSerializer().write(character, out);
			return out.toString().getBytes(Charset.forName("UTF-8"));
		} catch (Exception e) {
			logger.error("Failed generating XML for char",e);
			throw new DecodeEncodeException(e);
		}
	}

	//-------------------------------------------------------------------
	public static CoriolisCharacter load(InputStream in) throws DecodeEncodeException {
		try {
			CoriolisCharacter ret = ruleset.getSerializer().read(CoriolisCharacter.class, in);
			ret.setRuleset(ruleset);
			logger.info("Character successfully loaded: "+ret);

			List<Modification> toProcess = new ArrayList<>();
			List<CharacterProcessor> steps = Arrays.asList(
					new CalculateDerivedAttributes(),
					new ApplyTalentModifications()
					);
			for (CharacterProcessor step : steps) {
				toProcess = step.process(ret, toProcess);
			}
//			ruleset.getUtilities().applyAfterLoadModifications(ret);
//			loadEquipmentModifications(ret);
			return ret;
		} catch (Exception e) {
			logger.fatal("Failed loading character: "+e,e);
			throw new DecodeEncodeException(e);
		}
	}

	//-------------------------------------------------------------------
	public static CoriolisCharacter load(byte[] raw) throws DecodeEncodeException {
		return load( new ByteArrayInputStream( raw ));
	}

}
