module coriolis.core {
	exports org.prelle.coriolis;
	exports org.prelle.coriolis.persist;
	exports org.prelle.coriolis.charproc;

	opens org.prelle.coriolis;

	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires simple.persist;
	requires transitive yearzeroengine.core;
	requires de.rpgframework.products;
}